﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytm_generyczny
{
    public enum SposobWybierania
    {
        Najmniejszy,
        Najwiekszy
    }

    class ZbiorOsobnikow
    {
        private static readonly Random Rand = new Random();

        public readonly List<Osobnik> Osobniki = new List<Osobnik>();
        public Func<List<double>, double> Ocena;
        public SposobWybierania SposobWybierania;

        public ZbiorOsobnikow() { }
        //konstruktor tworzacy zbior osobnikow
        public ZbiorOsobnikow(int lo, int lbnp, int lp, double zdmin, double zdmax)
        {
            for (uint i = 0; i < lo; i++)
                Osobniki.Add(new Osobnik(lbnp, lp, zdmin, zdmax));
        }
        //funkcja zwracajaca wygrany osobnik zalezna od rozmiaru turnieju
        public Osobnik SelekcjaTurniejowa(int rozmiarTurnieju)
        {
            List<Osobnik> wylosowaniOsobnicy = new List<Osobnik>();

            wylosowaniOsobnicy
                .AddRange(Osobniki.OrderBy((osobnik) => Rand.Next())
                .Take(rozmiarTurnieju));

            wylosowaniOsobnicy = wylosowaniOsobnicy
                .OrderBy((osobnik) => Ocena(osobnik.DekodujWszystkieParametry())).ToList();

            if (SposobWybierania == SposobWybierania.Najmniejszy)
                return new Osobnik(wylosowaniOsobnicy.First());
            else
                return new Osobnik(wylosowaniOsobnicy.Last());

        }
        //funkcja tworzaca zbior wygranych osobnikow z turnieju
        public ZbiorOsobnikow Turniej(int rozmiarTurnieju, int iloscOsobnikow)
        {
            ZbiorOsobnikow zbior = new ZbiorOsobnikow();

            zbior.Ocena = Ocena;
            zbior.SposobWybierania = SposobWybierania;

            for (int i = 0; i < iloscOsobnikow; i++)
                zbior.Osobniki.Add(SelekcjaTurniejowa(rozmiarTurnieju));

            return zbior;
        }
        //funkcja do krzyzowania osobnikow
        public void Krzyzowanie(int o1, int o2)
        {
            if (o1 >= Osobniki.Count || o2 >= Osobniki.Count)
                throw new Exception("Zbyt duże indeksy");

            (Osobnik os1, Osobnik os2) = Osobnik.Krzyzowanie(Osobniki[o1], Osobniki[o2]);

            Osobniki[o1] = os1;
            Osobniki[o2] = os2;
        }
        //funkcja wybierajaca najlepsza wartosc
        public Osobnik HotDeck()
        {
            List<Osobnik> wszyscyOsobnicy = new List<Osobnik>();
            wszyscyOsobnicy.AddRange(Osobniki);

            wszyscyOsobnicy = wszyscyOsobnicy
                .OrderBy((osobnik) => Ocena(osobnik.DekodujWszystkieParametry())).ToList();

            if (SposobWybierania == SposobWybierania.Najmniejszy)
                return new Osobnik(wszyscyOsobnicy.First());
            else
                return new Osobnik(wszyscyOsobnicy.Last());

        }
        //funkcja wybierajaca najlepsza wartosc funkcji przystosowania
        public (double Najlepszy, double Sredni) NajlepszyISredniWynik()
        {
            List<double> wszystkieOceny = Osobniki.Select(osobnik => Ocena(osobnik.DekodujWszystkieParametry())).ToList();

            double srednia = wszystkieOceny.Average();

            if (SposobWybierania == SposobWybierania.Najmniejszy)
                return ( wszystkieOceny.Min(), srednia);
            else
                return ( wszystkieOceny.Max(), srednia);
        }
    }
}
