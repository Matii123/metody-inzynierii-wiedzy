﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytm_generyczny
{
    class Osobnik
    {
        private static readonly Random Rand = new Random();

        private readonly List<bool> Chromosomy = new List<bool>();
        private int Lbnp;
        private double ZdMin, ZdMax;

        //stworzenie osobnika
        public Osobnik(int lbnp, int lp, double zdmin, double zdmax)
        {
            Lbnp = lbnp;
            ZdMin = zdmin;
            ZdMax = zdmax;

            //wylosowanie i dodanie chromosomow
            for (int i = 0; i < lbnp * lp; i++)
                Chromosomy.Add(Rand.Next(0, 2) == 0);
        }
        //skopiowanie osobnika
        public Osobnik(Osobnik osobnik)
        {
            Chromosomy.AddRange(osobnik.Chromosomy);
            Lbnp = osobnik.Lbnp;
            ZdMin = osobnik.ZdMin;
            ZdMax = osobnik.ZdMax;
        }
        //dekodowanie jednego parametru
        public double DekodujParametr(int numer_parametru)
        {
            if (Chromosomy.Count / Lbnp <= numer_parametru)
                throw new Exception("Zbyt duży numer parametru");

            double zd = ZdMax - ZdMin;

            int ctmp = 0;

            for(int i = 0; i < Lbnp; i++)
            {
                //przesuniecie bitowe w lewo
                ctmp <<= 1;

                //sprawdzanie czy bit ma wartosc 1
                if (Chromosomy[numer_parametru * Lbnp + i])
                    ctmp += 1;
            }

            //zworcenie wyniku dekodowania
            return ZdMin + (ctmp / (Math.Pow(2, Lbnp) - 1) * zd);
        }
        //dekodowanie wszystkich parametrow
        public List<double> DekodujWszystkieParametry()
        {
            List<double> parametry = new List<double>();

            //dodanie wszystkich dekodowanych parametrow do listy 
            for (int i = 0; i < Chromosomy.Count / Lbnp; i++)
                parametry.Add(DekodujParametr(i));

            return parametry;
        }
        //operacja mutacji jednopunktowej
        public void Mutacja()
        {
            //wylosowanie punktu do operacji mutacji
            int punkt = Rand.Next(0, Chromosomy.Count);

            Chromosomy[punkt] = !Chromosomy[punkt];
        }
        //operacja krzyzowania osobnikow
        public static (Osobnik Os1, Osobnik Os2) Krzyzowanie(Osobnik o1, Osobnik o2)
        {
            //wylosowanie punktu ciecia
            int punkt = Rand.Next(0, o1.Chromosomy.Count - 1);

            //kopie osobnikow
            Osobnik os1 = new Osobnik(o1);
            Osobnik os2 = new Osobnik(o2);

            os1.Chromosomy.Clear();
            //wybranie pierwszej czesci z osobnika 1 oraz drugiej czesci z osobnika 2
            os1.Chromosomy.AddRange(o1.Chromosomy.Take(punkt + 1));
            os1.Chromosomy.AddRange(o2.Chromosomy.Skip(punkt + 1));

            os2.Chromosomy.Clear();
            //wybranie pierwszej czesci z osobnika 2 oraz drugiej czesci z osobnika 1
            os2.Chromosomy.AddRange(o2.Chromosomy.Take(punkt + 1));
            os2.Chromosomy.AddRange(o1.Chromosomy.Skip(punkt + 1));

            return (os1, os2);
        }
    }
}
