﻿
namespace Algorytm_generyczny
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbOsobniki = new System.Windows.Forms.Label();
            this.TbOsobniki = new System.Windows.Forms.TextBox();
            this.LbParametrow = new System.Windows.Forms.Label();
            this.TbParametry = new System.Windows.Forms.TextBox();
            this.LbMax = new System.Windows.Forms.Label();
            this.TbMax = new System.Windows.Forms.TextBox();
            this.LbMin = new System.Windows.Forms.Label();
            this.LbLBNP = new System.Windows.Forms.Label();
            this.TbMin = new System.Windows.Forms.TextBox();
            this.TbLBNP = new System.Windows.Forms.TextBox();
            this.LbTytyl = new System.Windows.Forms.Label();
            this.BtZad3 = new System.Windows.Forms.Button();
            this.LbIteracje = new System.Windows.Forms.Label();
            this.TbIteracje = new System.Windows.Forms.TextBox();
            this.LbRozmiar = new System.Windows.Forms.Label();
            this.TbRozmiar = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.RtbWynik = new System.Windows.Forms.RichTextBox();
            this.BtZad2 = new System.Windows.Forms.Button();
            this.BtZad1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LbOsobniki
            // 
            this.LbOsobniki.AutoSize = true;
            this.LbOsobniki.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbOsobniki.Location = new System.Drawing.Point(37, 212);
            this.LbOsobniki.Name = "LbOsobniki";
            this.LbOsobniki.Size = new System.Drawing.Size(119, 16);
            this.LbOsobniki.TabIndex = 38;
            this.LbOsobniki.Text = "Ilosc osobnikow";
            // 
            // TbOsobniki
            // 
            this.TbOsobniki.Location = new System.Drawing.Point(162, 208);
            this.TbOsobniki.Name = "TbOsobniki";
            this.TbOsobniki.Size = new System.Drawing.Size(136, 20);
            this.TbOsobniki.TabIndex = 37;
            this.TbOsobniki.Text = "13";
            // 
            // LbParametrow
            // 
            this.LbParametrow.AutoSize = true;
            this.LbParametrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbParametrow.Location = new System.Drawing.Point(363, 94);
            this.LbParametrow.Name = "LbParametrow";
            this.LbParametrow.Size = new System.Drawing.Size(138, 16);
            this.LbParametrow.TabIndex = 36;
            this.LbParametrow.Text = "Liczba parametrow";
            // 
            // TbParametry
            // 
            this.TbParametry.Location = new System.Drawing.Point(507, 93);
            this.TbParametry.Name = "TbParametry";
            this.TbParametry.Size = new System.Drawing.Size(136, 20);
            this.TbParametry.TabIndex = 35;
            this.TbParametry.Text = "9";
            // 
            // LbMax
            // 
            this.LbMax.AutoSize = true;
            this.LbMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbMax.Location = new System.Drawing.Point(120, 175);
            this.LbMax.Name = "LbMax";
            this.LbMax.Size = new System.Drawing.Size(36, 16);
            this.LbMax.TabIndex = 34;
            this.LbMax.Text = "Max";
            // 
            // TbMax
            // 
            this.TbMax.Location = new System.Drawing.Point(162, 171);
            this.TbMax.Name = "TbMax";
            this.TbMax.Size = new System.Drawing.Size(136, 20);
            this.TbMax.TabIndex = 33;
            this.TbMax.Text = "10";
            // 
            // LbMin
            // 
            this.LbMin.AutoSize = true;
            this.LbMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbMin.Location = new System.Drawing.Point(124, 136);
            this.LbMin.Name = "LbMin";
            this.LbMin.Size = new System.Drawing.Size(32, 16);
            this.LbMin.TabIndex = 32;
            this.LbMin.Text = "Min";
            // 
            // LbLBNP
            // 
            this.LbLBNP.AutoSize = true;
            this.LbLBNP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbLBNP.Location = new System.Drawing.Point(109, 97);
            this.LbLBNP.Name = "LbLBNP";
            this.LbLBNP.Size = new System.Drawing.Size(47, 16);
            this.LbLBNP.TabIndex = 31;
            this.LbLBNP.Text = "LBNP";
            // 
            // TbMin
            // 
            this.TbMin.Location = new System.Drawing.Point(162, 132);
            this.TbMin.Name = "TbMin";
            this.TbMin.Size = new System.Drawing.Size(136, 20);
            this.TbMin.TabIndex = 30;
            this.TbMin.Text = "-10";
            // 
            // TbLBNP
            // 
            this.TbLBNP.Location = new System.Drawing.Point(162, 93);
            this.TbLBNP.Name = "TbLBNP";
            this.TbLBNP.Size = new System.Drawing.Size(136, 20);
            this.TbLBNP.TabIndex = 29;
            this.TbLBNP.Text = "4";
            // 
            // LbTytyl
            // 
            this.LbTytyl.AutoSize = true;
            this.LbTytyl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbTytyl.Location = new System.Drawing.Point(320, 40);
            this.LbTytyl.Name = "LbTytyl";
            this.LbTytyl.Size = new System.Drawing.Size(91, 24);
            this.LbTytyl.TabIndex = 39;
            this.LbTytyl.Text = "Projekt 4";
            // 
            // BtZad3
            // 
            this.BtZad3.Location = new System.Drawing.Point(205, 349);
            this.BtZad3.Name = "BtZad3";
            this.BtZad3.Size = new System.Drawing.Size(93, 30);
            this.BtZad3.TabIndex = 40;
            this.BtZad3.Text = "Zad3";
            this.BtZad3.UseVisualStyleBackColor = true;
            this.BtZad3.Click += new System.EventHandler(this.BtZad3_Click);
            // 
            // LbIteracje
            // 
            this.LbIteracje.AutoSize = true;
            this.LbIteracje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbIteracje.Location = new System.Drawing.Point(409, 133);
            this.LbIteracje.Name = "LbIteracje";
            this.LbIteracje.Size = new System.Drawing.Size(92, 16);
            this.LbIteracje.TabIndex = 42;
            this.LbIteracje.Text = "Ilosc iteracji";
            // 
            // TbIteracje
            // 
            this.TbIteracje.Location = new System.Drawing.Point(507, 132);
            this.TbIteracje.Name = "TbIteracje";
            this.TbIteracje.Size = new System.Drawing.Size(136, 20);
            this.TbIteracje.TabIndex = 41;
            this.TbIteracje.Text = "100";
            // 
            // LbRozmiar
            // 
            this.LbRozmiar.AutoSize = true;
            this.LbRozmiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbRozmiar.Location = new System.Drawing.Point(382, 172);
            this.LbRozmiar.Name = "LbRozmiar";
            this.LbRozmiar.Size = new System.Drawing.Size(119, 16);
            this.LbRozmiar.TabIndex = 43;
            this.LbRozmiar.Text = "Rozmiar turnieju";
            // 
            // TbRozmiar
            // 
            this.TbRozmiar.Location = new System.Drawing.Point(507, 171);
            this.TbRozmiar.Name = "TbRozmiar";
            this.TbRozmiar.Size = new System.Drawing.Size(136, 20);
            this.TbRozmiar.TabIndex = 44;
            this.TbRozmiar.Text = "3";
            // 
            // RtbWynik
            // 
            this.RtbWynik.Location = new System.Drawing.Point(366, 230);
            this.RtbWynik.Name = "RtbWynik";
            this.RtbWynik.Size = new System.Drawing.Size(277, 194);
            this.RtbWynik.TabIndex = 47;
            this.RtbWynik.Text = "";
            // 
            // BtZad2
            // 
            this.BtZad2.Location = new System.Drawing.Point(205, 300);
            this.BtZad2.Name = "BtZad2";
            this.BtZad2.Size = new System.Drawing.Size(93, 30);
            this.BtZad2.TabIndex = 48;
            this.BtZad2.Text = "Zad2";
            this.BtZad2.UseVisualStyleBackColor = true;
            this.BtZad2.Click += new System.EventHandler(this.BtZad2_Click);
            // 
            // BtZad1
            // 
            this.BtZad1.Location = new System.Drawing.Point(205, 253);
            this.BtZad1.Name = "BtZad1";
            this.BtZad1.Size = new System.Drawing.Size(93, 30);
            this.BtZad1.TabIndex = 49;
            this.BtZad1.Text = "Zad1";
            this.BtZad1.UseVisualStyleBackColor = true;
            this.BtZad1.Click += new System.EventHandler(this.BtZad1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 450);
            this.Controls.Add(this.BtZad1);
            this.Controls.Add(this.BtZad2);
            this.Controls.Add(this.RtbWynik);
            this.Controls.Add(this.TbRozmiar);
            this.Controls.Add(this.LbRozmiar);
            this.Controls.Add(this.LbIteracje);
            this.Controls.Add(this.TbIteracje);
            this.Controls.Add(this.BtZad3);
            this.Controls.Add(this.LbTytyl);
            this.Controls.Add(this.LbOsobniki);
            this.Controls.Add(this.TbOsobniki);
            this.Controls.Add(this.LbParametrow);
            this.Controls.Add(this.TbParametry);
            this.Controls.Add(this.LbMax);
            this.Controls.Add(this.TbMax);
            this.Controls.Add(this.LbMin);
            this.Controls.Add(this.LbLBNP);
            this.Controls.Add(this.TbMin);
            this.Controls.Add(this.TbLBNP);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbOsobniki;
        private System.Windows.Forms.TextBox TbOsobniki;
        private System.Windows.Forms.Label LbParametrow;
        private System.Windows.Forms.TextBox TbParametry;
        private System.Windows.Forms.Label LbMax;
        private System.Windows.Forms.TextBox TbMax;
        private System.Windows.Forms.Label LbMin;
        private System.Windows.Forms.Label LbLBNP;
        private System.Windows.Forms.TextBox TbMin;
        private System.Windows.Forms.TextBox TbLBNP;
        private System.Windows.Forms.Label LbTytyl;
        private System.Windows.Forms.Button BtZad3;
        private System.Windows.Forms.Label LbIteracje;
        private System.Windows.Forms.TextBox TbIteracje;
        private System.Windows.Forms.Label LbRozmiar;
        private System.Windows.Forms.TextBox TbRozmiar;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.RichTextBox RtbWynik;
        private System.Windows.Forms.Button BtZad2;
        private System.Windows.Forms.Button BtZad1;
    }
}

