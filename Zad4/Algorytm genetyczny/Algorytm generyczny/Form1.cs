﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Algorytm_generyczny
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtZad1_Click(object sender, EventArgs e)
        {
            int liczba_osobnikow = int.Parse(TbOsobniki.Text);
            int liczba_parametrow = int.Parse(TbParametry.Text);
            int liczbaIteracji = int.Parse(TbIteracje.Text);
            int rozmiarTurnieju = int.Parse(TbRozmiar.Text);
            int LBNP = int.Parse(TbLBNP.Text);
            double zdmin = double.Parse(TbMin.Text);
            double zdmax = double.Parse(TbMax.Text);

            double najlepszy, srednia;

            if (LBNP >= 3 && liczbaIteracji >= 20 && liczba_osobnikow % 2 == 1 && liczba_osobnikow >= 9)
            {
                //stworzenie zbioru nowych osobnikow
                ZbiorOsobnikow zbiorOsobnikow = new ZbiorOsobnikow(liczba_osobnikow, LBNP, liczba_parametrow, zdmin, zdmax);
                //dekodowanie dla kazdego osobnika watosci x1 i x2 i wyliczenie funkcji przystosowania
                zbiorOsobnikow.Ocena = (parametry) =>
                {
                    double x1 = parametry[0];
                    double x2 = parametry[1];

                    return Math.Sin(x1 * 0.05) + Math.Sin(x2 * 0.05) + 0.4 * Math.Sin(x1 * 0.15) * Math.Sin(x2 * 0.15);
                };

                //najistotniejsze sa najwieksze wartosci
                zbiorOsobnikow.SposobWybierania = SposobWybierania.Najwiekszy;

                //przypisanie najlepszej i sredniej wartosci funkcji przystosowania
                (najlepszy, srednia) = zbiorOsobnikow.NajlepszyISredniWynik();
                //wypisanie najlepszej i sredniej wartosci
                RtbWynik.Text = "Pula startowa\r\n";
                RtbWynik.Text += $"Najlepszy: {najlepszy}\r\nŚredni: {srednia}\r\n\r\n";

                for (int i = 0; i < liczbaIteracji; i++)
                {
                    //stworzenie turnieju o liczbie osobnikow - 1
                    ZbiorOsobnikow nowyZbiorOsobnikow = zbiorOsobnikow.Turniej(rozmiarTurnieju, liczba_osobnikow - 1);
                    //wykonanie mutacji na kazdym osobniku
                    nowyZbiorOsobnikow.Osobniki.ForEach(osobnik => osobnik.Mutacja());
                    //wybranie i dodanie najlepszego osobnika z operacji hotdeck
                    nowyZbiorOsobnikow.Osobniki.Add(zbiorOsobnikow.HotDeck());
                    //przypisanie najlepszej i sredniej wartosci funkcji przystosowania
                    (najlepszy, srednia) = zbiorOsobnikow.NajlepszyISredniWynik();
                    //wypisanie najlepszej i sredniej wartosci
                    RtbWynik.Text += $"Iteracja {i + 1}\r\n";
                    RtbWynik.Text += $"Najlepszy: {najlepszy}\r\nŚredni: {srednia}\r\n\r\n";
                    //przypisanie zbioru nowych osobnikow do starego zbioru 
                    zbiorOsobnikow = nowyZbiorOsobnikow;
                }
                //wypisanie najlepszego rozwiazania o podanych parametrach
                var najlepszeParametry = zbiorOsobnikow.HotDeck().DekodujWszystkieParametry();
                RtbWynik.Text += $"Najlepsze rozwiązanie\r\n";
                for (int i = 0; i < liczba_parametrow; i++)
                    RtbWynik.Text += $"Parametr {i + 1}: {najlepszeParametry[i]}\r\n";
            }
            else
            {
                MessageBox.Show("Podales bledne dane");
            }
        }

        private void BtZad2_Click(object sender, EventArgs e)
        {
            int liczba_osobnikow = int.Parse(TbOsobniki.Text);
            int liczba_parametrow = int.Parse(TbParametry.Text);
            int liczbaIteracji = int.Parse(TbIteracje.Text);
            int rozmiarTurnieju = int.Parse(TbRozmiar.Text);
            int LBNP = int.Parse(TbLBNP.Text);
            double zdmin = double.Parse(TbMin.Text);
            double zdmax = double.Parse(TbMax.Text);

            double najlepszy, srednia;

            //baza probek sinusik
            Dictionary<double, double> funkcjaTestowa = new Dictionary<double, double>()
            {
                {-1.00000, 0.59554 },
                {-0.80000, 0.58813 },
                {-0.60000, 0.64181 },
                {-0.40000, 0.68587 },
                {-0.20000, 0.44783 },
                {0.00000, 0.40836  },
                {0.20000, 0.38241  },
                {0.40000, -0.05933 },
                {0.60000, -0.12478 },
                {0.80000, -0.36847 },
                {1.00000, -0.39935 },
                {1.20000, -0.50881 },
                {1.40000, -0.63435 },
                {1.60000, -0.59979 },
                {1.80000, -0.64107 },
                {2.00000, -0.51808 },
                {2.20000, -0.38127 },
                {2.40000, -0.12349 },
                {2.60000, -0.09624 },
                {2.80000, 0.27893  },
                {3.00000, 0.48965  },
                {3.20000, 0.33089  },
                {3.40000, 0.70615  },
                {3.60000, 0.53342  },
                {3.80000, 0.43321  },
                {4.00000, 0.64790  },
                {4.20000, 0.48834  },
                {4.40000, 0.18440  },
                {4.60000, -0.02389 },
                {4.80000, -0.10261 },
                {5.00000, -0.33594 },
                {5.20000, -0.35101 },
                {5.40000, -0.62027 },
                {5.60000, -0.55719 },
                {5.80000, -0.66377 },
                { 6.00000, -0.62740}
            };
            if (LBNP >= 4 && liczbaIteracji >= 100 && liczba_osobnikow == 13 && rozmiarTurnieju == 3)
            {
                //stworzenie zbioru nowych osobnikow
                ZbiorOsobnikow zbior = new ZbiorOsobnikow(liczba_osobnikow, LBNP, liczba_parametrow, zdmin, zdmax);
                //najistotniejsze sa najmniejsze wartosci
                zbior.SposobWybierania = SposobWybierania.Najmniejszy;
                //dekodowanie dla kazdego osobnika watosci pa, pb, pc i wyliczenie funkcji przystosowania
                zbior.Ocena = (parametry) =>
                {
                    double pa = parametry[0];
                    double pb = parametry[1];
                    double pc = parametry[2];

                    //funkcja z parametrami pa, pb i pc
                    Func<double, double> f = (x) => pa * Math.Sin(pb * x + pc);

                    double suma = 0;

                    //suma kwadratow bledow dla wszystkich probek
                    foreach (var punkt in funkcjaTestowa)
                        suma += Math.Pow(punkt.Value - f(punkt.Key), 2);

                    return suma;
                };

                //przypisanie najlepszej i sredniej wartosci funkcji przystosowania
                (najlepszy, srednia) = zbior.NajlepszyISredniWynik();
                //wypisanie najlepszej i sredniej wartosci
                RtbWynik.Text = "Pula startowa\r\n";
                RtbWynik.Text += $"Najlepszy: {najlepszy}\r\nŚredni: {srednia}\r\n\r\n";

                for (int i = 0; i < liczbaIteracji; i++)
                {
                    //stworzenie turnieju o liczbie osobnikow - 1
                    ZbiorOsobnikow nowyZbiorOsobnikow = zbior.Turniej(rozmiarTurnieju, liczba_osobnikow - 1);
                    //wykonanie operacji krzyzowania
                    nowyZbiorOsobnikow.Krzyzowanie(0, 1);
                    nowyZbiorOsobnikow.Krzyzowanie(2, 3);
                    nowyZbiorOsobnikow.Krzyzowanie(8, 9);
                    nowyZbiorOsobnikow.Krzyzowanie(liczba_osobnikow - 3, liczba_osobnikow - 2);
                    //wykonanie mutacji na osobnikach od 5 do ostatniego
                    nowyZbiorOsobnikow.Osobniki.Skip(4).ToList().ForEach(osobnik => osobnik.Mutacja());
                    //wybranie i dodanie najlepszego osobnika z operacji hotdeck
                    nowyZbiorOsobnikow.Osobniki.Add(zbior.HotDeck());
                    //przypisanie najlepszej i sredniej wartosci funkcji przystosowania
                    (najlepszy, srednia) = zbior.NajlepszyISredniWynik();
                    //wypisanie najlepszego rozwiazania o podanych argumentach
                    RtbWynik.Text += $"Iteracja {i + 1}\r\n";
                    RtbWynik.Text += $"Najlepszy: {najlepszy}\r\nŚredni: {srednia}\r\n\r\n";
                    //przypisanie zbioru nowych osobnikow do starego zbioru 
                    zbior = nowyZbiorOsobnikow;
                }
                //wypisanie najlepszego rozwiazania o podanych parametrach
                var najlepszeParametry = zbior.HotDeck().DekodujWszystkieParametry();
                RtbWynik.Text += $"Najlepsze rozwiązanie\r\n";
                for (int i = 0; i < liczba_parametrow; i++)
                    RtbWynik.Text += $"Parametr {i + 1}: {najlepszeParametry[i]}\r\n";
            }
            else
            {
                MessageBox.Show("Podales bledne dane");
            }
        }

        private void BtZad3_Click(object sender, EventArgs e)
        {
            int liczba_osobnikow = int.Parse(TbOsobniki.Text);
            int liczba_parametrow = int.Parse(TbParametry.Text);
            int liczbaIteracji = int.Parse(TbIteracje.Text);
            int rozmiarTurnieju = int.Parse(TbRozmiar.Text);
            int LBNP = int.Parse(TbLBNP.Text);
            double zdmin = double.Parse(TbMin.Text);
            double zdmax = double.Parse(TbMax.Text);

            double najlepszy, srednia;

            if (LBNP >= 4 && liczbaIteracji >= 100 && liczba_osobnikow == 13 && rozmiarTurnieju == 3)
            {
                //stworzenie zbioru nowych osobnikow
                ZbiorOsobnikow zbior = new ZbiorOsobnikow(liczba_osobnikow, LBNP, liczba_parametrow, zdmin, zdmax);
                //najistotniejsze sa najmniejsze wartosci
                zbior.SposobWybierania = SposobWybierania.Najmniejszy;
                //dekodowanie dla kazdego osobnika watosci 9 parametrow i wyliczenie funkcji przystosowania
                zbior.Ocena = (parametry) =>
                {
                    double w11 = parametry[0];
                    double w12 = parametry[1];
                    double w13 = parametry[2];
                    double w21 = parametry[3];
                    double w22 = parametry[4];
                    double w23 = parametry[5];
                    double w31 = parametry[6];
                    double w32 = parametry[7];
                    double w33 = parametry[8];

                    //funkcja aktywacji
                    Func<double, double> aktywacja = (x) => 1 / (1 + Math.Exp(-1 * x));

                    double suma = 0;
                    //petla dla kazdego wejscia
                    for (int i = 0; i < 4; i++)
                    {
                        double we1 = i / 2 == 1 ? 1 : 0;
                        double we2 = i % 2 == 1 ? 1 : 0;

                        double n1 = w11 + w12 * we1 + w13 * we2;
                        double n2 = w21 + w22 * we1 + w23 * we2;

                        double a1 = aktywacja(n1);
                        double a2 = aktywacja(n2);

                        double n3 = w31 + w32 * a1 + w33 * a2;
                        double a3 = aktywacja(n3);

                        //suma kwadratow bledow dla 4 probek
                        suma += Math.Pow(((we1 == 1 ^ we2 == 1) ? 1 : 0) - a3, 2);
                    }

                    return suma;
                };

                //przypisanie najlepszej i sredniej wartosci funkcji przystosowania
                (najlepszy, srednia) = zbior.NajlepszyISredniWynik();
                RtbWynik.Text = "Pula startowa\r\n";
                RtbWynik.Text += $"Najlepszy: {najlepszy}\r\nŚredni: {srednia}\r\n\r\n";

                for (int i = 0; i < liczbaIteracji; i++)
                {
                    //stworzenie turnieju o liczbie osobnikow - 1
                    ZbiorOsobnikow nowyZbiorOsobnikow = zbior.Turniej(rozmiarTurnieju, liczba_osobnikow - 1);
                    //wykonanie operacji krzyzowania
                    nowyZbiorOsobnikow.Krzyzowanie(0, 1);
                    nowyZbiorOsobnikow.Krzyzowanie(2, 3);
                    nowyZbiorOsobnikow.Krzyzowanie(8, 9);
                    nowyZbiorOsobnikow.Krzyzowanie(liczba_osobnikow - 3, liczba_osobnikow - 2);
                    //wykonanie mutacji na osobnikach od 5 do ostatniego
                    nowyZbiorOsobnikow.Osobniki.Skip(4).ToList().ForEach(osobnik => osobnik.Mutacja());
                    //wybranie i dodanie najlepszego osobnika z operacji hotdeck
                    nowyZbiorOsobnikow.Osobniki.Add(zbior.HotDeck());
                    //przypisanie najlepszej i sredniej wartosci funkcji przystosowania
                    (najlepszy, srednia) = zbior.NajlepszyISredniWynik();
                    //wypisanie najlepszego rozwiazania o podanych argumentach
                    RtbWynik.Text += $"Iteracja {i + 1}\r\n";
                    RtbWynik.Text += $"Najlepszy: {najlepszy}\r\nŚredni: {srednia}\r\n\r\n";
                    //przypisanie zbioru nowych osobnikow do starego zbioru 
                    zbior = nowyZbiorOsobnikow;
                }

                //wypisanie najlepszego rozwiazania o podanych parametrach
                var najlepszeParametry = zbior.HotDeck().DekodujWszystkieParametry();
                RtbWynik.Text += $"Najlepsze rozwiązanie\r\n";
                for (int i = 0; i < liczba_parametrow; i++)
                    RtbWynik.Text += $"Parametr {i + 1}: {najlepszeParametry[i]}\r\n";
            }
            else
            {
                MessageBox.Show("Podales bledne dane");
            }
        }
    }
}
