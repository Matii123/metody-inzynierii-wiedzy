﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacja
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string sciezka;
        string[,] tablica_dwuwymiarowa;
        double[,] tablica_normalizowana;
        double[,] tablica_odleglosci;
        string sciezka2 = @".\plik.txt";
        List<string> nazwy_kolumn = new List<string>();
        List<string> usuniete_kolumny = new List<string>();

        //zmienna wskazujaca klase decyzyjna
        int decyzja = 0;
        string str_decyzja = "A1";
        int wariant = 0;

        int maks_k1 = 0;
        int maks_k2 = 0;

        private void BtnWczytaj_Click(object sender, EventArgs e)
        {
            nazwy_kolumn.Clear();
            openFileDialog1.ShowDialog();
            sciezka = openFileDialog1.FileName;
            LbWczytaj.Text = sciezka;

            try
            {
                tablica_dwuwymiarowa = WczytajDane(sciezka);
                GenerujPlikKonfiguracyjny(tablica_dwuwymiarowa);

            }
            catch
            {
                LbWczytaj.Text = "Brak";
                MessageBox.Show("Inne rozszerzenie");
            }

        }

        private void BtnWypisz_Click(object sender, EventArgs e)
        {
            //Sprawdzenie czy plik został wczytany
            if (LbWczytaj.Text == "Brak")
            {
                MessageBox.Show("Wybierz plik, który chcesz wczytać");
            }
            else
            {
                WypiszDane(tablica_dwuwymiarowa);
                GenerujPlikKonfiguracyjny(tablica_dwuwymiarowa);
            }
        }
        private void BtnDecyzyjna_Click(object sender, EventArgs e)
        {
            if (CbLista.SelectedItem == null)
            {
                MessageBox.Show("Wybierz klase decyzyjna lub wypisz dane");
            }
            else
            {
                dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.White;
                decyzja = CbLista.SelectedIndex;
                str_decyzja = CbLista.SelectedItem.ToString();

                WypiszDane(tablica_dwuwymiarowa);
                GenerujPlikKonfiguracyjny(tablica_dwuwymiarowa);
            }
        }
        private void BtnNormalizuj_Click(object sender, EventArgs e)
        {
            //Sprawdzenie czy plik został wczytany
            if (LbWczytaj.Text == "Brak")
            {
                MessageBox.Show("Wybierz plik, który chcesz wczytać");
            }
            else
            {
                double poczZakresu = 0;
                double konZakresu = 1;
                try
                {
                    //zmienne do zakresu normalizacji
                    poczZakresu = double.Parse(TbZakres1.Text);
                    konZakresu = double.Parse(TbZakres2.Text);
                }
                catch
                {
                    MessageBox.Show("Zły format danych");
                }

                try
                {

                    NormalizujDane(tablica_dwuwymiarowa, poczZakresu, konZakresu);
                    string[,] tablica_normalizowana_str = new string[tablica_normalizowana.GetLength(0), tablica_normalizowana.GetLength(1)];

                    for (int i = 0; i < tablica_normalizowana.GetLength(0); i++)
                    {
                        for (int j = 0; j < tablica_normalizowana.GetLength(1); j++)
                        {
                            tablica_normalizowana_str[i, j] = tablica_normalizowana[i, j].ToString();
                        }
                    }
                    WypiszDane(tablica_normalizowana_str);
                }
                catch
                {
                    MessageBox.Show("Wybierz plik ponownie lub wypisz dane");
                }
            }
        }
        private void BtnWczytajConfiga_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            sciezka2 = openFileDialog2.FileName;
            try
            {
                WczytajConfig(sciezka2);
            }
            catch
            {
                MessageBox.Show("Zły format pliku lub błędne dane w pliku konfiguracyjnym");
            }
        }
        private void BtnUsun_Click(object sender, EventArgs e)
        {
            if (CbUsun.SelectedItem == null)
            {
                MessageBox.Show("Wybierz kolumne do usuniecia lub wypisz dane");
            }
            else
            {
                int kolumna = CbUsun.SelectedIndex;
                tablica_dwuwymiarowa = UsunKolumne(kolumna);
                if (kolumna == decyzja)
                {
                    str_decyzja = dataGridView1.Columns[decyzja + 1].HeaderText;
                }
                WypiszDane(tablica_dwuwymiarowa);
                GenerujPlikKonfiguracyjny(tablica_dwuwymiarowa);
            }
        }
        private void BtOdleglosci_Click(object sender, EventArgs e)
        {
                if (tablica_odleglosci != null)
                {
                    WypiszOdleglosci();
                }
                else
                {
                    MessageBox.Show("Sklasyfikuj element");
                }
        }

        private void BtKlasyfikuj_Click(object sender, EventArgs e)
        {
            if (CbMetryka.SelectedItem != null || CbWariant.SelectedItem != null || CbObiekty.SelectedItem != null)
            {
                int k = int.Parse(TbParametrk.Text);
                string wyborMetryka = CbMetryka.SelectedItem.ToString();
                int wybor = CbObiekty.SelectedIndex;

                double poczZakresu = double.Parse(TbZakres1.Text);
                double konZakresu = double.Parse(TbZakres2.Text);
                NormalizujDane(tablica_dwuwymiarowa, poczZakresu, konZakresu);

                double[] probka_testowa = new double[tablica_normalizowana.GetLength(1) - 1];
                double[,] probka_wzorcowa = new double[tablica_normalizowana.GetLength(0) - 1, tablica_normalizowana.GetLength(1)];

                int licz = 0;
                //Probka testowa bez klasy decyzyjnej
                for (int j = 0; j < tablica_normalizowana.GetLength(1); j++)
                {
                    //Sprawdzenie ktorą kolumne należy usunąć
                    if (j == decyzja)
                    {
                        licz++;
                    }
                    else
                    {
                        for (int i = 0; i < tablica_normalizowana.GetLength(0); i++)
                        {
                            probka_testowa[j - licz] = tablica_normalizowana[wybor, j];
                        }
                    }
                }

                int licznik = 0;
                //Usuniecie probki z bazy wzorcowej 
                for (int i = 0; i < tablica_normalizowana.GetLength(0); i++)
                {
                    for (int j = 0; j < tablica_normalizowana.GetLength(1); j++)
                    {
                        if (wybor == i)
                        {
                            i++;
                            j = 0;
                            licznik = 1;
                        }
                        if (i != tablica_normalizowana.GetLength(0))
                        {
                            probka_wzorcowa[i - licznik, j] = tablica_normalizowana[i, j];
                        }
                    }
                }

                wariant = CbWariant.SelectedIndex;

                ObliczMaksymalneK(probka_wzorcowa);
                if((wariant == 0 && maks_k1 >= k) || (wariant == 1 && maks_k2 >= k))
                {
                    TbWynik.Text = KlasyfikujObiekt(probka_wzorcowa, probka_testowa, k, wyborMetryka);
                }
                else
                {
                    MessageBox.Show("Podales bledne k");
                }
                
            }
            else
            {
                MessageBox.Show("Wypelnij wszystkie pola");
            }
        }
        private void BtSkutecznosc_Click(object sender, EventArgs e)
        {
            if (CbMetryka3.SelectedItem != null || CbWariant3.SelectedItem != null)
            {
                string wyborMetryka = CbMetryka3.SelectedItem.ToString();
                int k = int.Parse(TbParametrk3.Text);
                wariant = CbWariant3.SelectedIndex;

                if (tablica_dwuwymiarowa != null)
                {
                    double poczZakresu = double.Parse(TbZakres1.Text);
                    double konZakresu = double.Parse(TbZakres2.Text);
                    NormalizujDane(tablica_dwuwymiarowa, poczZakresu, konZakresu);

                    ObliczMaksymalneK(tablica_normalizowana);
                    //Sprawdzenie parametru k
                    if ((CbWariant3.SelectedIndex == 0 && maks_k1 > k) || (CbWariant3.SelectedIndex == 1 && maks_k2 > k))
                    {
                        Skutecznosc(tablica_normalizowana, wyborMetryka, k);
                    }
                    else
                    {
                        MessageBox.Show("Podales bledne k");
                    }
                }
                else
                {
                    MessageBox.Show("Wczytaj dane");
                }
            }
            else
            {
                MessageBox.Show("Wypelnij wszystkie pola");
            }
        }
        private void BtnKonkretnyNor_Click(object sender, EventArgs e)
        {
            if (CbMetryka2.SelectedItem != null || CbWariant2.SelectedItem != null)
            {
                int k = int.Parse(TbParametrk2.Text);
                string wyborMetryka = CbMetryka2.SelectedItem.ToString();

                double poczZakresu = double.Parse(TbZakres1.Text);
                double konZakresu = double.Parse(TbZakres2.Text);
                NormalizujDane(tablica_dwuwymiarowa, poczZakresu, konZakresu);

                double[] probka_testowa = new double[tablica_normalizowana.GetLength(1) - 1];
                char sperarator = '-';
                string[] tekst = (TbKonkretny.Text).Split(sperarator);
                bool nieprawidlowe = false;


                //Sprawdzenie czy liczby mieszcza sie w zakresie
                for (int i = 0; i < tekst.Length; i++)
                {
                    if ((double.Parse(tekst[i]) < poczZakresu || (double.Parse(tekst[i]) > konZakresu)))
                    {
                        nieprawidlowe = true;
                    }
                }

                //Sprawdzenie czy dlugosc zgadza sie
                if (probka_testowa.Length == tekst.Length && nieprawidlowe == false)
                {
                    for (int i = 0; i < probka_testowa.Length; i++)
                    {
                        probka_testowa[i] = double.Parse(tekst[i]);
                    }

                    wariant = CbWariant2.SelectedIndex;
                    ObliczMaksymalneK(tablica_normalizowana);
                    if ((wariant == 0 && maks_k1 >= k) || (wariant == 1 && maks_k2 >= k))
                    {
                        TbWynik2.Text = KlasyfikujObiekt(tablica_normalizowana, probka_testowa, k, wyborMetryka);
                    }
                    else
                    {
                        MessageBox.Show("Podales bledne k");
                    }
                }
                else
                {
                    MessageBox.Show("Niepoprawna dlugosc obiektu lub dane wykraczaja poza zakres");
                }
            }
            else
            {
                MessageBox.Show("Wypelnij wszystkie pola");
            }
        }

        private void BtnKonkretnyNieNor_Click(object sender, EventArgs e)
        {
            if (CbMetryka2.SelectedItem != null || CbWariant2.SelectedItem != null)
            {
                int k = int.Parse(TbParametrk2.Text);
                string wyborMetryka = CbMetryka2.SelectedItem.ToString();

                double poczZakresu = double.Parse(TbZakres1.Text);
                double konZakresu = double.Parse(TbZakres2.Text);
                NormalizujDane(tablica_dwuwymiarowa, poczZakresu, konZakresu);

                string[] probka_testowa = new string[tablica_normalizowana.GetLength(1) - 1];

                char sperarator = '-';
                string[] tekst = (TbKonkretny.Text).Split(sperarator);
                int index = 0;


                if (probka_testowa.Length == tekst.Length)
                {
                    for (int i = 0; i < probka_testowa.Length; i++)
                    {
                        probka_testowa[i] = tekst[i];
                    }

                    double[] probka_testowa_nor = new double[probka_testowa.Length];
                    string[] daneConfig = File.ReadAllLines(sciezka2);
                    double[] maxi = new double[tablica_normalizowana.GetLength(1) - 1];
                    double[] mini = new double[tablica_normalizowana.GetLength(1) - 1];
                    double[] przedzial = new double[tablica_normalizowana.GetLength(1) - 1];
                    bool niepoprawne = false;
                    double pom_dbl;

                    //Zamiana znakow na cyfry przez config
                    for (int i = 13; i < daneConfig.Length; i++)
                    {
                        string[] pom = daneConfig[i].Split(' ');
                        if ((i - 13) != decyzja)
                        {
                            if (pom[1] == "Nienumeryczny")
                            {
                                for (int j = 2; j < pom.Length; j += 3)
                                {
                                    if (probka_testowa[index] == pom[j])
                                    {
                                        probka_testowa[index] = pom[j + 2];
                                    }
                                }
                            }
                            index++;
                        }
                    }
                    index = 0;
                    //Wybieranie minimalnych i maksymalnych wartosci w configu
                    for (int i = 13; i < daneConfig.Length; i++)
                    {
                        string[] pom = daneConfig[i].Split(' ');
                        if ((i - 13) != decyzja)
                        {
                            if (pom[1] == "Numeryczny")
                            {
                                mini[index] = double.Parse(pom[2]);
                                maxi[index] = double.Parse(pom[3]);
                            }
                            else
                            {
                                mini[index] = double.Parse(pom[4]);
                                maxi[index] = double.Parse(pom[pom.Length - 1]);
                            }
                            przedzial[index] = maxi[index] - mini[index];
                            index++;
                        }
                    }

                    //Normalizowacja probki
                    for (int i = 0; i < probka_testowa_nor.Length; i++)
                    {
                        if (double.TryParse(probka_testowa[i], out pom_dbl))
                        {
                            if (maxi[i] >= double.Parse(probka_testowa[i]) && mini[i] <= double.Parse(probka_testowa[i]))
                            {
                                probka_testowa_nor[i] = Math.Round((poczZakresu + (((double.Parse(probka_testowa[i]) - mini[i]) / przedzial[i]) * (konZakresu - poczZakresu))), 3);
                            }
                            else
                            {
                                //Sprawdznie czy nie zostala wprowadzona liczba 
                                niepoprawne = true;
                            }
                        }
                        else
                        {
                            niepoprawne = true;
                        }
                    }
                    //Wstrzymanie klasyfikacji i wyswietlenie komunikatu
                    if (niepoprawne == false)
                    {
                        wariant = CbWariant2.SelectedIndex;
                        ObliczMaksymalneK(tablica_normalizowana);
                        if ((wariant == 0 && maks_k1 >= k) || (wariant == 1 && maks_k2 >= k))
                        {
                            TbWynik2.Text = KlasyfikujObiekt(tablica_normalizowana, probka_testowa_nor, k, wyborMetryka);
                        }
                        else
                        {
                            MessageBox.Show("Podales bledne k");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Podales za duze lub za male liczby lub (niepoprawny znak)");
                    }
                }
                else
                {
                    MessageBox.Show("Niepoprawna dlugosc obiektu");
                }
            }
            else
            {
                MessageBox.Show("Wypelnij wszystkie pola");
            }

        }
        string[,] WczytajDane(string sciezka_wczytana)
        {
            char separator = ',';
            string[] lines = File.ReadAllLines(sciezka_wczytana);
            List<int> listaUsuwania = new List<int>();

            //Sprawdzenie separatora w danym pliku 
            if (lines[0].Contains(" "))
            {
                separator = ' ';
            }
            else if (lines[0].Contains(","))
            {
                separator = ',';
            }
            else if (lines[0].Contains(":"))
            {
                separator = ':';
            }
            else if (lines[0].Contains(";"))
            {
                separator = ';';
            }
            else if (lines[0].Contains("\t"))
            {
                separator = '\t';
            }

            string[] slowo = lines[0].Split(separator);
            tablica_dwuwymiarowa = new string[lines.Length, slowo.Length];

            //Stworzenie dwuwymiarowej tablicy i zastąpienie kropki przecinkiem
            for (int i = 0; i < lines.Length; i++)
            {
                slowo = lines[i].Split(separator);
                for (int j = 0; j < slowo.Length; j++)
                {
                    slowo[j] = slowo[j].Replace('.', ',');
                    tablica_dwuwymiarowa[i, j] = slowo[j];
                }
            }

            //Wyszukanie rekordów z nieznanymi wartosciami
            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
                {
                    if (tablica_dwuwymiarowa[i, j] == "?")
                    {
                        listaUsuwania.Add(i);
                        i++;
                        j = 0;
                    }

                }
            }

            //tablica z nowymi wartosciami
            string[,] tablica_koncowa = new string[tablica_dwuwymiarowa.GetLength(0) - listaUsuwania.Count, tablica_dwuwymiarowa.GetLength(1)];

            //licznik pozwalajacy na ustawienie wartosci w prawidlowych miejscach
            int licznik = 0;

            //usuniecie rekordów z nieznanymi wartosciami
            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
                {
                    if (listaUsuwania.Contains(i))
                    {
                        i++;
                        j = 0;
                        licznik++;
                    }
                    tablica_koncowa[i - licznik, j] = tablica_dwuwymiarowa[i, j];
                }
            }

            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            CbLista.Items.Clear();
            CbUsun.Items.Clear();
            CbMetryka.Items.Clear();
            CbMetryka2.Items.Clear();
            CbMetryka3.Items.Clear();
            CbWariant.Items.Clear();
            CbWariant2.Items.Clear();
            CbWariant3.Items.Clear();
            usuniete_kolumny.Clear();

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_koncowa.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView1.Columns.Add(column);
            }

            for (int i = 0; i < tablica_koncowa.GetLength(0); i++)
            {
                dataGridView1.Rows.Add();
            }

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_koncowa.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_koncowa.GetLength(1); j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = tablica_koncowa[i, j];
                }
            }

            //Dodadnie obiektow do comboboxa
            for (int i = 0; i < tablica_koncowa.GetLength(0); i++)
            {
                CbObiekty.Items.Add(i);
            }

            //Nazewnictwo kolumn i zaznaczenie klasy decyzyjnej
            for (int i = 0; i < tablica_koncowa.GetLength(1); i++)
            {

                dataGridView1.Columns[i].HeaderText = "A" + (i + 1);
                nazwy_kolumn.Add("A" + (i + 1));
                CbLista.Items.Add("A" + (i + 1));
                CbUsun.Items.Add("A" + (i + 1));
            }

            dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
            dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.LightBlue;

            //Dodanie do Comboboxa metryk 
            CbMetryka.Items.Add("Euklimedesa");
            CbMetryka.Items.Add("Manhattana");
            CbMetryka.Items.Add("Czebyszewa");
            CbMetryka.Items.Add("Minkowskiego");
            CbMetryka.Items.Add("Logarytmem");

            CbMetryka2.Items.Add("Euklimedesa");
            CbMetryka2.Items.Add("Manhattana");
            CbMetryka2.Items.Add("Czebyszewa");
            CbMetryka2.Items.Add("Minkowskiego");
            CbMetryka2.Items.Add("Logarytmem");

            CbMetryka3.Items.Add("Euklimedesa");
            CbMetryka3.Items.Add("Manhattana");
            CbMetryka3.Items.Add("Czebyszewa");
            CbMetryka3.Items.Add("Minkowskiego");
            CbMetryka3.Items.Add("Logarytmem");

            CbWariant.Items.Add("Wariant 1");
            CbWariant.Items.Add("Wariant 2");

            CbWariant2.Items.Add("Wariant 1");
            CbWariant2.Items.Add("Wariant 2");

            CbWariant3.Items.Add("Wariant 1");
            CbWariant3.Items.Add("Wariant 2");

            return tablica_koncowa;
        }
        void WypiszDane(string[,] tablica_wczytana)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            CbLista.Items.Clear();
            CbUsun.Items.Clear();
            CbObiekty.Items.Clear();

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView1.Columns.Add(column);
            }

            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            {
                dataGridView1.Rows.Add();
            }

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = tablica_wczytana[i, j];
                }
            }

            //Dodadnie obiektow do comboboxa
            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
            {
                CbObiekty.Items.Add(i);
            }

            //Nazewnictwo kolumn i zaznaczenie klasy decyzyjnej
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {

                dataGridView1.Columns[i].HeaderText = nazwy_kolumn[i];
                CbLista.Items.Add(nazwy_kolumn[i]);
                CbUsun.Items.Add(nazwy_kolumn[i]);
            }

            dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
            dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.LightBlue;
            str_decyzja = nazwy_kolumn[decyzja];

        }
        void NormalizujDane(string[,] tablica_wczytana, double poczZakresu, double konZakresu)
        {
            tablica_normalizowana = new double[tablica_wczytana.GetLength(0), tablica_wczytana.GetLength(1)];
            double[] maxi = new double[tablica_wczytana.GetLength(1)];
            double[] mini = new double[tablica_wczytana.GetLength(1)];
            double[] przedzial = new double[tablica_wczytana.GetLength(1)];
            int licznik = 1;
            double pom_dbl;
            Dictionary<string, string> slownik = new Dictionary<string, string>();

            //Zastapienie znakow liczbami
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                {
                    //Sprawdzanie czy wartosc jest liczba
                    if (!(double.TryParse(tablica_wczytana[i, j], out pom_dbl)))
                    {
                        //Sprawdzanie czy istnieje dany klucz
                        if (slownik.ContainsKey(tablica_wczytana[i, j]))
                        {
                            foreach (var znak in slownik)
                            {
                                if (znak.Key == tablica_wczytana[i, j])
                                {
                                    tablica_wczytana[i, j] = znak.Value;
                                }
                            }
                        }
                        else
                        {
                            slownik.Add(tablica_wczytana[i, j], licznik.ToString());
                            tablica_wczytana[i, j] = licznik.ToString();
                            licznik++;
                        }
                    }
                }
                licznik = 1;
                slownik.Clear();
            }

            //Wyznaczenie wartosci maksymalnych i minimalnych do normalizacji
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                maxi[j] = double.Parse(tablica_wczytana[1, j]);
                mini[j] = double.Parse(tablica_wczytana[1, j]);
                for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                {
                    if (double.Parse(tablica_wczytana[i, j]) > maxi[j])
                    {
                        maxi[j] = double.Parse(tablica_wczytana[i, j]);
                    }
                    if (double.Parse(tablica_wczytana[i, j]) < mini[j])
                    {
                        mini[j] = double.Parse(tablica_wczytana[i, j]);
                    }
                }

            }

            //Wyzanczenie przedzialu do normalizacji
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {
                przedzial[i] = maxi[i] - mini[i];
            }

            //Normalizacja tablicy
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                {
                    if (decyzja != j)
                    {
                        tablica_normalizowana[i, j] = Math.Round((poczZakresu + (((double.Parse(tablica_wczytana[i, j]) - mini[j]) / przedzial[j]) * (konZakresu - poczZakresu))), 3);
                    }
                    else
                    {
                        tablica_normalizowana[i, j] = double.Parse(tablica_wczytana[i, j]);
                    }
                }
            }
        }

        void ObliczOdleglosc(double[,] tablica_wczytana, double[] probka, string wyborMetryka)
        {
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Refresh();

            tablica_odleglosci = new double[tablica_wczytana.GetLength(0), 2];
            double[,] tablica_bezKlasy = new double[tablica_wczytana.GetLength(0), tablica_wczytana.GetLength(1) - 1];
            int licz = 0;

            //Tablica znormalizowana bez klasy decyzyjnej
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                //Sprawdzenie ktorą kolumne należy usunąć
                if (j == decyzja)
                {
                    licz++;
                }
                else
                {
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        tablica_bezKlasy[i, j - licz] = tablica_wczytana[i, j];
                    }
                }
            }

            //Zapisanie klas decyzyjnych
            for (int i = 0; i < tablica_odleglosci.GetLength(0); i++)
            {
               tablica_odleglosci[i, 0] = tablica_wczytana[i, decyzja];
            }

            double suma;

            if (wyborMetryka == "Euklimedesa")
            {
                //Obliczenie metryki Euklimedesa
                for (int i = 0; i < tablica_bezKlasy.GetLength(0); i++)
                {
                    suma = 0;
                    for (int j = 0; j < tablica_bezKlasy.GetLength(1); j++)
                    {
                        suma += Math.Pow((probka[j] - tablica_bezKlasy[i, j]), 2);
                    }
                    tablica_odleglosci[i, 1] = Math.Sqrt(suma);
                }
            }
            else if (wyborMetryka == "Manhattana")
            {
                //Obliczenie metryki Manhattan
                for (int i = 0; i < tablica_bezKlasy.GetLength(0); i++)
                {
                    suma = 0;
                    for (int j = 0; j < tablica_bezKlasy.GetLength(1); j++)
                    {
                        suma += Math.Abs(probka[j] - tablica_bezKlasy[i, j]);
                    }
                    tablica_odleglosci[i, 1] = suma;
                }
            }
            else if (wyborMetryka == "Czebyszewa")
            {
                //Obliczenie metryki Czebyszewa
                for (int i = 0; i < tablica_bezKlasy.GetLength(0); i++)
                {
                    double Max_Czebyszew = 0;
                    for (int j = 0; j < tablica_bezKlasy.GetLength(1); j++)
                    {
                        if (Max_Czebyszew < Math.Abs(probka[j] - tablica_bezKlasy[i, j]))
                        {
                            Max_Czebyszew = Math.Abs(probka[j] - tablica_bezKlasy[i, j]);
                        }
                    }
                    tablica_odleglosci[i, 1] = Max_Czebyszew;
                }
            }
            else if (wyborMetryka == "Minkowskiego")
            {
                //Obliczenie metryki Minkowskiego
                double m = tablica_bezKlasy.GetLength(1);
                for (int i = 0; i < tablica_bezKlasy.GetLength(0); i++)
                {
                    suma = 0;
                    for (int j = 0; j < tablica_bezKlasy.GetLength(1); j++)
                    {
                        suma += Math.Pow((Math.Abs(probka[j] - tablica_bezKlasy[i, j])), m);
                    }

                    tablica_odleglosci[i, 1] = Math.Pow(suma, (1 / m));
                }
            }
            else if (wyborMetryka == "Logarytmem")
            {
                //Obliczenie metryki logarytmem
                for (int i = 0; i < tablica_bezKlasy.GetLength(0); i++)
                {
                    suma = 0;
                    for (int j = 0; j < tablica_bezKlasy.GetLength(1); j++)
                    {
                        if (tablica_bezKlasy[i, j] > 0)
                        {
                            suma += Math.Abs(Math.Log(probka[j] - Math.Log(tablica_bezKlasy[i, j])));
                        }
                    }
                    tablica_odleglosci[i, 1] = suma;
                }
            }

        }
        string KlasyfikujWariant(double[,] probka_wzorcowa, double[] probka_testowa, int k, string wyborMetryka)
        {

            ObliczOdleglosc(probka_wzorcowa, probka_testowa, wyborMetryka);

            List<List<double>> lista_odleglosci = new List<List<double>>();
            for (int i = 0; i < tablica_odleglosci.GetLength(0); i++)
            {
                List<double> row = new List<double>();
                for (int j = 0; j < tablica_odleglosci.GetLength(1); j++)
                {
                    row.Add(tablica_odleglosci[i, j]);
                }
                lista_odleglosci.Add(row);
            }

            var posortowane = lista_odleglosci.OrderBy(i => i[1]).Take(k).ToList();
            var slownik = new Dictionary<double, int>();

            foreach (var i in posortowane)
            {
                if (slownik.ContainsKey(i[0]))
                {
                    slownik[i[0]]++;
                }
                else
                {
                    slownik[i[0]] = 1;
                }
            }

            double decyzja_k = 0;
            int licznik = 0;

            //Decyzja i ilosc
            foreach (var i in slownik)
            {
                if (licznik < i.Value)
                {
                    licznik = i.Value;
                    decyzja_k = i.Key;
                }
            }

            string wynik = "";
            wynik = decyzja_k.ToString();

            //Sprawdzenie czy liczb klas decyzyjnych jest taka sama
            foreach (var i in slownik)
            {
                if (licznik == i.Value && decyzja_k != i.Key)
                {
                    wynik = "Brak";
                }
            }
            return wynik;
        }

        string KlasyfikujWariant2(double[,] probka_wzorcowa, double[] probka_testowa, int k, string wyborMetryka)
        {
            //Lista klas decyzyjnych
            List<double> lista_klas = new List<double>();

            ObliczOdleglosc(probka_wzorcowa, probka_testowa, wyborMetryka);

            //Lista klas decyzyjnych
            for (int i = 0; i < tablica_odleglosci.GetLength(0); i++)
            {
                if (!(lista_klas.Contains(tablica_odleglosci[i, 0])))
                {
                    lista_klas.Add(tablica_odleglosci[i, 0]);
                }
            }

            List<List<double>> lista_odleglosci = new List<List<double>>();
            for (int i = 0; i < tablica_odleglosci.GetLength(0); i++)
            {
                List<double> row = new List<double>();
                for (int j = 0; j < tablica_odleglosci.GetLength(1); j++)
                {
                    row.Add(tablica_odleglosci[i, j]);
                }
                lista_odleglosci.Add(row);
            }
          
            List<double> lista = new List<double> {int.MaxValue};

            for (int x = 0; x < lista_klas.Count(); x++)
            {
                var posortowane = lista_odleglosci.Where(i => i[0] == lista_klas[x]).ToList().OrderBy(i => i[1]).Take(k).ToList();
                double suma = 0;
                foreach(var i in posortowane)
                {
                    suma += i[1];
                }
                if(suma < lista[0])
                {
                    lista = new List<double> {suma,lista_klas[x]};
                }
                else if(suma == lista[0])
                {
                    lista.Add(suma);
                    lista.Add(lista_klas[x]);
                }
            }

            string wynik = "";
            if(lista.Count != 2)
            {
                wynik = "Brak";
            }
            else
            {
                wynik = lista[1].ToString();
            }

            return wynik;
        }
        void WypiszOdleglosci()
        {
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Refresh();

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_odleglosci.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView2.Columns.Add(column);
            }

            for (int i = 0; i < tablica_odleglosci.GetLength(0); i++)
            {
                dataGridView2.Rows.Add();
            }

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_odleglosci.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_odleglosci.GetLength(1); j++)
                {
                    dataGridView2.Rows[i].Cells[j].Value = tablica_odleglosci[i, j];
                }
            }

            //dataGridView2.Columns[0].HeaderText = "Index";
            dataGridView2.Columns[0].HeaderText = "Klasa decyzyjna";
            dataGridView2.Columns[1].HeaderText = "Odległość";
        }

        void Skutecznosc(double[,] probka_wzorcowa, string wyborMetryka, int k)
        {
            dataGridView3.Rows.Clear();
            dataGridView3.Columns.Clear();
            dataGridView3.Refresh();

            double[,] tablica_skutecznosci = new double[1, 3];
            double liczba_obiektow = probka_wzorcowa.GetLength(0);
            //licznik zliczajacy liczbe klasyfikacji
            double licznik = 0;
            //licznik zliczajacy prawidlowe klasyfikacje
            double licznik_prawidlowy = 0;

            string wynik = "";

            tablica_skutecznosci[0, 0] = k;
            //Obliczenie odleglosci od wybranego obiektu
            for (int i = 0; i < probka_wzorcowa.GetLength(0); i++)
            {
                wynik = Klasyfiikuj_bez(probka_wzorcowa, i, k, wyborMetryka);

                //Sprawdzenie czy wartosc da sie sklasyfikowac
                if (wynik != "Brak")
                {
                    licznik++;
                    //Sprawdzenie czy klasa jest taka sama(poprawnosc)               
                    if (probka_wzorcowa[i, decyzja].ToString() == wynik)
                    {
                        licznik_prawidlowy++;
                    }
                }
            }
            tablica_skutecznosci[0, 1] = licznik / liczba_obiektow;
            tablica_skutecznosci[0, 2] = licznik_prawidlowy / licznik;

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_skutecznosci.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView3.Columns.Add(column);
            }

            for (int i = 0; i < tablica_skutecznosci.GetLength(0); i++)
            {
                dataGridView3.Rows.Add();
            }

            dataGridView3.Columns[0].HeaderText = "Parametr k";
            dataGridView3.Columns[1].HeaderText = "Pokrycie";
            dataGridView3.Columns[2].HeaderText = "Skutecznosc";

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_skutecznosci.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_skutecznosci.GetLength(1); j++)
                {
                    dataGridView3.Rows[i].Cells[j].Value = tablica_skutecznosci[i, j];
                }
            }
        }
        string Klasyfiikuj_bez(double[,] probka_wzorcowa, int wybor, int k, string wyborMetryka)
        {
            string wynik = "";
            double[,] probka_wzorcowa_bez = new double[probka_wzorcowa.GetLength(0) - 1, probka_wzorcowa.GetLength(1)];
            double[] probka_testowa = new double[probka_wzorcowa.GetLength(1) - 1];

            int licz = 0;
            //Probka bez klasy decyzjnej
            for (int j = 0; j < probka_wzorcowa.GetLength(1); j++)
            {
                if (j == decyzja)
                {
                    licz++;
                }
                else
                {
                    probka_testowa[j - licz] = probka_wzorcowa[wybor, j];
                }
            }

            int licznik = 0;

            //Usuniecie probki z bazy wzorcowej 
            for (int i = 0; i < probka_wzorcowa.GetLength(0); i++)
            {
                for (int j = 0; j < probka_wzorcowa.GetLength(1); j++)
                {
                    if (wybor == i)
                    {
                        i++;
                        j = 0;
                        licznik = 1;
                    }
                    if (i != probka_wzorcowa.GetLength(0))
                    {
                        probka_wzorcowa_bez[i - licznik, j] = probka_wzorcowa[i, j];
                    }
                }
            }

            ObliczMaksymalneK(probka_wzorcowa_bez);
            wynik = KlasyfikujObiekt(probka_wzorcowa_bez, probka_testowa, k, wyborMetryka);
            return wynik;
        }
        void WczytajConfig(string sciezka_wczytana)
        {
            string[] daneConfig = File.ReadAllLines(sciezka_wczytana);
            string plik = "";
            for (int i = 0; i < daneConfig.Length; i++)
            {
                plik = plik + daneConfig[i] + "\n";
            }

            //mozliwosc ustawienia klasy decyzyjnej
            string[] danedecyzja = daneConfig[4].Split(' ');

            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(1); i++)
            {
                if ((danedecyzja[0] == dataGridView1.Columns[i].HeaderText))
                {
                    decyzja = i;
                }
            }

            //mozliwosc ustawienia zakresu normalizacji
            string[] dane_normalizacja = daneConfig[6].Split(' ');
            double poczZakresu = double.Parse(dane_normalizacja[0]);
            double konZakresu = double.Parse(dane_normalizacja[1]);
            TbZakres1.Text = poczZakresu.ToString();
            TbZakres2.Text = konZakresu.ToString();

            //mozliwosc wybrania atrybutów do normalizacji
            string[] atrybuty_normaliazja = daneConfig[8].Split(' ');
            List<int> lista_atrybutow = new List<int>();

            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(1); i++)
            {
                for (int j = 0; j < atrybuty_normaliazja.GetLength(0); j++)
                {
                    if (nazwy_kolumn[i] == atrybuty_normaliazja[j])
                    {
                        lista_atrybutow.Add(i);
                    }
                }
            }

            string[,] tablica_pomniejszona;

            tablica_pomniejszona = WybierzDoNormalizacji(tablica_dwuwymiarowa, lista_atrybutow);
            NormalizujDane(tablica_pomniejszona, poczZakresu, konZakresu);

            string[,] tablica_normalizowana_str = new string[tablica_normalizowana.GetLength(0), tablica_normalizowana.GetLength(1)];

            for (int i = 0; i < tablica_normalizowana.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_normalizowana.GetLength(1); j++)
                {
                    tablica_normalizowana_str[i, j] = tablica_normalizowana[i, j].ToString();
                }
            }
            WypiszDane(tablica_normalizowana_str);

            for (int i = 0; i < tablica_normalizowana.GetLength(1); i++)
            {
                dataGridView1.Columns[i].HeaderText = atrybuty_normaliazja[i];
            }

            dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
            dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.LightBlue;

        }
        string[,] UsunKolumne(int kolumna)
        {
            string[,] tablica_koncowa = new string[tablica_dwuwymiarowa.GetLength(0), tablica_dwuwymiarowa.GetLength(1) - 1];
            int licznik = 0;
            usuniete_kolumny.Add(nazwy_kolumn[kolumna]);
            nazwy_kolumn.RemoveAt(kolumna);

            for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
            {
                //Sprawdzenie ktorą kolumne należy usunąć
                if (j == kolumna)
                {
                    licznik = 1;
                }
                else
                {
                    for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
                    {
                        tablica_koncowa[i, j - licznik] = tablica_dwuwymiarowa[i, j];
                    }
                }
            }

            //Jesli usuwana kolumna jest przed klasa decyzyjna to przesuwamy w lewa strone
            if (kolumna < decyzja)
            {
                decyzja--;
            }

            return tablica_koncowa;
        }
        void GenerujPlikKonfiguracyjny(string[,] tablica_wczytana)
        {
            string plik;
            double[] maxi = new double[tablica_wczytana.GetLength(1)];
            double[] mini = new double[tablica_wczytana.GetLength(1)];
            double[] przedzial = new double[tablica_wczytana.GetLength(1)];
            int licznik = 1;
            string pom = "";

            Dictionary<string, string> slownik = new Dictionary<string, string>();
            StreamWriter sw = new StreamWriter(@".\plik.txt");

            //Dodanie do ilosci wierszy i kolumn
            plik = "PLIK KONFIGURACYJNY" + "\n";
            plik = plik + "Ilosc atrybutów" + "\n";
            plik = plik + tablica_wczytana.GetLength(1) + "\n";

            plik = plik + "Klasa decyzyjna" + "\n";
            plik = plik + str_decyzja + "\n";

            plik = plik + "Przedział normalizacji" + "\n";
            plik = plik + TbZakres1.Text + " " + TbZakres2.Text + "\n";

            plik = plik + "Kolumny normalizowane" + "\n";
            for (int i = 0; i < nazwy_kolumn.Count(); i++)
            {
                plik = plik + nazwy_kolumn[i] + " ";
            }

            if (!(usuniete_kolumny.Count() == 0))
            {
                plik = plik + "\n";
                plik = plik + "Kolumny usuniete" + "\n";
                for (int i = 0; i < usuniete_kolumny.Count(); i++)
                {
                    plik = plik + usuniete_kolumny[i] + " ";
                }
            }
            else
            {
                plik = plik + "\n";
                plik = plik + "\n";
            }

            plik = plik + "\n" + "INFORMACJE O DANYCH" + "\n";
            plik = plik + "Atrybut(nazwa, typ, maks, mini, przedzial)" + "\n";
            //Wyznaczenie wartosci maksymalnych, minimalnych i przedzialu
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                //Sprawdzenie czy kolumna jest numeryczna
                if (double.TryParse(tablica_wczytana[1, j], out maxi[j]))
                {
                    maxi[j] = double.Parse(tablica_wczytana[1, j]);
                    mini[j] = double.Parse(tablica_wczytana[1, j]);
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        if (double.Parse(tablica_wczytana[i, j]) > maxi[j])
                        {
                            maxi[j] = double.Parse(tablica_wczytana[i, j]);
                        }
                        if (double.Parse(tablica_wczytana[i, j]) < mini[j])
                        {
                            mini[j] = double.Parse(tablica_wczytana[i, j]);
                        }
                    }
                    przedzial[j] = maxi[j] - mini[j];
                    //Dodanie wartosci do pliku
                    plik = plik + dataGridView1.Columns[j].HeaderText + " " + "Numeryczny" + " " + mini[j] + " " + maxi[j] + " " + przedzial[j] + "\n";

                }
                else
                {
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        //Sprawdzanie czy istnieje dany klucz
                        if (!(slownik.ContainsKey(tablica_wczytana[i, j])))
                        {
                            slownik.Add(tablica_wczytana[i, j], licznik.ToString());
                            licznik++;
                        }
                    }
                    foreach (var znak in slownik)
                    {
                        pom = pom + " " + znak.Key + " => " + znak.Value;
                    }
                    plik = plik + dataGridView1.Columns[j].HeaderText + " " + "Nienumeryczny" + pom + "\n";
                    slownik.Clear();
                    pom = "";
                    licznik = 1;
                }

            }
            sw.Write(plik);
            sw.Close();

        }
        string[,] WybierzDoNormalizacji(string[,] tablica_wczytana, List<int> pominieteAtrybuty)
        {

            string[,] tablica_koncowa = new string[tablica_wczytana.GetLength(0), pominieteAtrybuty.Count()];
            int licznik = 0;
            //licznik pozwalajacy na przesuniecie klasy decyzynej
            int pomin = 0;

            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                //sprawdzenie ktorą kolumne należy usunąć
                if (!pominieteAtrybuty.Contains(j))
                {
                    licznik++;
                    if (j < decyzja)
                    {
                        pomin++;
                    }

                }
                else
                {
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        tablica_koncowa[i, j - licznik] = tablica_wczytana[i, j];
                    }
                }
            }
            decyzja -= pomin;
            str_decyzja = dataGridView1.Columns[decyzja].HeaderText;
            return tablica_koncowa;
        }
        void ObliczMaksymalneK(double[,] tablica_wczytana)
        {
            maks_k1 = tablica_wczytana.GetLength(0);

            double[] k_elementow2 = new double[tablica_wczytana.GetLength(0)];

            //Zapisanie klas decyzyjnych
            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            {
                k_elementow2[i] = tablica_wczytana[i, decyzja];
            }

            var slownik = new Dictionary<double, int>();

            //Zliczanie ile istnieje dany klucz
            foreach (var i in k_elementow2)
            {
                if (slownik.ContainsKey(i))
                {
                    slownik[i]++;
                }
                else
                {
                    slownik[i] = 1;
                }
            }

            int min = tablica_wczytana.GetLength(0);

            //Decyzja i ilosc
            foreach (var i in slownik)
            {
                if (min > i.Value)
                {
                    min = i.Value;
                }
            }
            maks_k2 = min;
        }
        string KlasyfikujObiekt(double[,] probka_wzorcowa, double[] probka_testowa, int k, string wyborMetryka)
        {
            string wynik = "";

            //Sprawdzenie wariantu
            if (wariant == 0)
            {
                wynik = KlasyfikujWariant(probka_wzorcowa, probka_testowa, k, wyborMetryka);
            }
            else if (wariant == 1)
            {
                wynik = KlasyfikujWariant2(probka_wzorcowa, probka_testowa, k, wyborMetryka);
            }

            return wynik;
        }
    }
}