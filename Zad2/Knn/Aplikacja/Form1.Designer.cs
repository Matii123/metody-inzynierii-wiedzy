﻿
namespace Aplikacja
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbWczytaj = new System.Windows.Forms.Label();
            this.BtnWczytaj = new System.Windows.Forms.Button();
            this.LbTytul = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.CbLista = new System.Windows.Forms.ComboBox();
            this.BtnDecyzyjna = new System.Windows.Forms.Button();
            this.TbZakres2 = new System.Windows.Forms.TextBox();
            this.TbZakres1 = new System.Windows.Forms.TextBox();
            this.LbZakresNormalizacji = new System.Windows.Forms.Label();
            this.BtnNormalizuj = new System.Windows.Forms.Button();
            this.BtnWypisz = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.BtOdleglosci = new System.Windows.Forms.Button();
            this.LbProbkaBaza = new System.Windows.Forms.Label();
            this.LbProbkaWlasna = new System.Windows.Forms.Label();
            this.TbParametrk = new System.Windows.Forms.TextBox();
            this.LbParametrk = new System.Windows.Forms.Label();
            this.TbWynik = new System.Windows.Forms.TextBox();
            this.LbWynik = new System.Windows.Forms.Label();
            this.BtKlasyfikuj = new System.Windows.Forms.Button();
            this.LbMetryka = new System.Windows.Forms.Label();
            this.CbMetryka = new System.Windows.Forms.ComboBox();
            this.CbMetryka2 = new System.Windows.Forms.ComboBox();
            this.LbMetryka2 = new System.Windows.Forms.Label();
            this.LbWynik2 = new System.Windows.Forms.Label();
            this.TbWynik2 = new System.Windows.Forms.TextBox();
            this.TbParametrk2 = new System.Windows.Forms.TextBox();
            this.LbParametrk2 = new System.Windows.Forms.Label();
            this.CbObiekty = new System.Windows.Forms.ComboBox();
            this.LbWybierz = new System.Windows.Forms.Label();
            this.LbStatystyka = new System.Windows.Forms.Label();
            this.CbMetryka3 = new System.Windows.Forms.ComboBox();
            this.LbMetryka3 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.LbWariant3 = new System.Windows.Forms.Label();
            this.CbWariant3 = new System.Windows.Forms.ComboBox();
            this.LbParametrk3 = new System.Windows.Forms.Label();
            this.TbParametrk3 = new System.Windows.Forms.TextBox();
            this.BtSkutecznosc = new System.Windows.Forms.Button();
            this.BtnUsun = new System.Windows.Forms.Button();
            this.BtnWczytajConfiga = new System.Windows.Forms.Button();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.CbUsun = new System.Windows.Forms.ComboBox();
            this.TbKonkretny = new System.Windows.Forms.TextBox();
            this.LbKonkretny = new System.Windows.Forms.Label();
            this.BtnKonkretnyNor = new System.Windows.Forms.Button();
            this.BtnKonkretnyNieNor = new System.Windows.Forms.Button();
            this.CbWariant = new System.Windows.Forms.ComboBox();
            this.LbWariant = new System.Windows.Forms.Label();
            this.CbWariant2 = new System.Windows.Forms.ComboBox();
            this.LbWariant2 = new System.Windows.Forms.Label();
            this.LbWszystkie = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // LbWczytaj
            // 
            this.LbWczytaj.AutoSize = true;
            this.LbWczytaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWczytaj.Location = new System.Drawing.Point(150, 80);
            this.LbWczytaj.Name = "LbWczytaj";
            this.LbWczytaj.Size = new System.Drawing.Size(39, 18);
            this.LbWczytaj.TabIndex = 4;
            this.LbWczytaj.Text = "Brak";
            // 
            // BtnWczytaj
            // 
            this.BtnWczytaj.Location = new System.Drawing.Point(27, 74);
            this.BtnWczytaj.Name = "BtnWczytaj";
            this.BtnWczytaj.Size = new System.Drawing.Size(114, 32);
            this.BtnWczytaj.TabIndex = 3;
            this.BtnWczytaj.Text = "Wczytaj plik";
            this.BtnWczytaj.UseVisualStyleBackColor = true;
            this.BtnWczytaj.Click += new System.EventHandler(this.BtnWczytaj_Click);
            // 
            // LbTytul
            // 
            this.LbTytul.AutoSize = true;
            this.LbTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbTytul.Location = new System.Drawing.Point(541, 27);
            this.LbTytul.Name = "LbTytul";
            this.LbTytul.Size = new System.Drawing.Size(106, 25);
            this.LbTytul.TabIndex = 7;
            this.LbTytul.Text = "Projekt 2";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // CbLista
            // 
            this.CbLista.FormattingEnabled = true;
            this.CbLista.Location = new System.Drawing.Point(310, 125);
            this.CbLista.Name = "CbLista";
            this.CbLista.Size = new System.Drawing.Size(121, 21);
            this.CbLista.TabIndex = 25;
            // 
            // BtnDecyzyjna
            // 
            this.BtnDecyzyjna.Location = new System.Drawing.Point(153, 118);
            this.BtnDecyzyjna.Name = "BtnDecyzyjna";
            this.BtnDecyzyjna.Size = new System.Drawing.Size(151, 32);
            this.BtnDecyzyjna.TabIndex = 24;
            this.BtnDecyzyjna.Text = "Wybierz klase decyzyjną";
            this.BtnDecyzyjna.UseVisualStyleBackColor = true;
            this.BtnDecyzyjna.Click += new System.EventHandler(this.BtnDecyzyjna_Click);
            // 
            // TbZakres2
            // 
            this.TbZakres2.Location = new System.Drawing.Point(312, 166);
            this.TbZakres2.Name = "TbZakres2";
            this.TbZakres2.Size = new System.Drawing.Size(68, 20);
            this.TbZakres2.TabIndex = 23;
            this.TbZakres2.Text = "2";
            // 
            // TbZakres1
            // 
            this.TbZakres1.Location = new System.Drawing.Point(238, 166);
            this.TbZakres1.Name = "TbZakres1";
            this.TbZakres1.Size = new System.Drawing.Size(68, 20);
            this.TbZakres1.TabIndex = 22;
            this.TbZakres1.Text = "1";
            // 
            // LbZakresNormalizacji
            // 
            this.LbZakresNormalizacji.AutoSize = true;
            this.LbZakresNormalizacji.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbZakresNormalizacji.Location = new System.Drawing.Point(26, 165);
            this.LbZakresNormalizacji.Name = "LbZakresNormalizacji";
            this.LbZakresNormalizacji.Size = new System.Drawing.Size(194, 18);
            this.LbZakresNormalizacji.TabIndex = 21;
            this.LbZakresNormalizacji.Text = "Wybierz zakres normalizacji";
            // 
            // BtnNormalizuj
            // 
            this.BtnNormalizuj.Location = new System.Drawing.Point(409, 159);
            this.BtnNormalizuj.Name = "BtnNormalizuj";
            this.BtnNormalizuj.Size = new System.Drawing.Size(114, 32);
            this.BtnNormalizuj.TabIndex = 20;
            this.BtnNormalizuj.Text = "Normalizuj dane";
            this.BtnNormalizuj.UseVisualStyleBackColor = true;
            this.BtnNormalizuj.Click += new System.EventHandler(this.BtnNormalizuj_Click);
            // 
            // BtnWypisz
            // 
            this.BtnWypisz.Location = new System.Drawing.Point(27, 118);
            this.BtnWypisz.Name = "BtnWypisz";
            this.BtnWypisz.Size = new System.Drawing.Size(114, 32);
            this.BtnWypisz.TabIndex = 19;
            this.BtnWypisz.Text = "Wypisz dane";
            this.BtnWypisz.UseVisualStyleBackColor = true;
            this.BtnWypisz.Click += new System.EventHandler(this.BtnWypisz_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(27, 241);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(496, 238);
            this.dataGridView1.TabIndex = 26;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(121, 564);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(276, 238);
            this.dataGridView2.TabIndex = 27;
            // 
            // BtOdleglosci
            // 
            this.BtOdleglosci.Location = new System.Drawing.Point(153, 819);
            this.BtOdleglosci.Name = "BtOdleglosci";
            this.BtOdleglosci.Size = new System.Drawing.Size(220, 32);
            this.BtOdleglosci.TabIndex = 29;
            this.BtOdleglosci.Text = "Wyświetl wszystkie odległości metryki";
            this.BtOdleglosci.UseVisualStyleBackColor = true;
            this.BtOdleglosci.Click += new System.EventHandler(this.BtOdleglosci_Click);
            // 
            // LbProbkaBaza
            // 
            this.LbProbkaBaza.AutoSize = true;
            this.LbProbkaBaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbProbkaBaza.Location = new System.Drawing.Point(794, 106);
            this.LbProbkaBaza.Name = "LbProbkaBaza";
            this.LbProbkaBaza.Size = new System.Drawing.Size(269, 25);
            this.LbProbkaBaza.TabIndex = 32;
            this.LbProbkaBaza.Text = "Klasyfikuj próbkę z bazy";
            // 
            // LbProbkaWlasna
            // 
            this.LbProbkaWlasna.AutoSize = true;
            this.LbProbkaWlasna.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbProbkaWlasna.Location = new System.Drawing.Point(794, 376);
            this.LbProbkaWlasna.Name = "LbProbkaWlasna";
            this.LbProbkaWlasna.Size = new System.Drawing.Size(273, 25);
            this.LbProbkaWlasna.TabIndex = 33;
            this.LbProbkaWlasna.Text = "Klasyfikuj próbkę własną";
            // 
            // TbParametrk
            // 
            this.TbParametrk.Location = new System.Drawing.Point(915, 147);
            this.TbParametrk.Name = "TbParametrk";
            this.TbParametrk.Size = new System.Drawing.Size(115, 20);
            this.TbParametrk.TabIndex = 35;
            this.TbParametrk.Text = "3";
            // 
            // LbParametrk
            // 
            this.LbParametrk.AutoSize = true;
            this.LbParametrk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbParametrk.Location = new System.Drawing.Point(828, 147);
            this.LbParametrk.Name = "LbParametrk";
            this.LbParametrk.Size = new System.Drawing.Size(81, 18);
            this.LbParametrk.TabIndex = 34;
            this.LbParametrk.Text = "Parametr k";
            // 
            // TbWynik
            // 
            this.TbWynik.Location = new System.Drawing.Point(917, 337);
            this.TbWynik.Name = "TbWynik";
            this.TbWynik.Size = new System.Drawing.Size(113, 20);
            this.TbWynik.TabIndex = 36;
            // 
            // LbWynik
            // 
            this.LbWynik.AutoSize = true;
            this.LbWynik.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWynik.Location = new System.Drawing.Point(860, 337);
            this.LbWynik.Name = "LbWynik";
            this.LbWynik.Size = new System.Drawing.Size(49, 18);
            this.LbWynik.TabIndex = 37;
            this.LbWynik.Text = "Wynik";
            // 
            // BtKlasyfikuj
            // 
            this.BtKlasyfikuj.Location = new System.Drawing.Point(915, 292);
            this.BtKlasyfikuj.Name = "BtKlasyfikuj";
            this.BtKlasyfikuj.Size = new System.Drawing.Size(114, 32);
            this.BtKlasyfikuj.TabIndex = 38;
            this.BtKlasyfikuj.Text = "Klasyfikuj obiekt";
            this.BtKlasyfikuj.UseVisualStyleBackColor = true;
            this.BtKlasyfikuj.Click += new System.EventHandler(this.BtKlasyfikuj_Click);
            // 
            // LbMetryka
            // 
            this.LbMetryka.AutoSize = true;
            this.LbMetryka.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbMetryka.Location = new System.Drawing.Point(848, 184);
            this.LbMetryka.Name = "LbMetryka";
            this.LbMetryka.Size = new System.Drawing.Size(61, 18);
            this.LbMetryka.TabIndex = 39;
            this.LbMetryka.Text = "Metryka";
            // 
            // CbMetryka
            // 
            this.CbMetryka.FormattingEnabled = true;
            this.CbMetryka.Location = new System.Drawing.Point(915, 184);
            this.CbMetryka.Name = "CbMetryka";
            this.CbMetryka.Size = new System.Drawing.Size(115, 21);
            this.CbMetryka.TabIndex = 40;
            // 
            // CbMetryka2
            // 
            this.CbMetryka2.FormattingEnabled = true;
            this.CbMetryka2.Location = new System.Drawing.Point(917, 454);
            this.CbMetryka2.Name = "CbMetryka2";
            this.CbMetryka2.Size = new System.Drawing.Size(117, 21);
            this.CbMetryka2.TabIndex = 47;
            // 
            // LbMetryka2
            // 
            this.LbMetryka2.AutoSize = true;
            this.LbMetryka2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbMetryka2.Location = new System.Drawing.Point(850, 454);
            this.LbMetryka2.Name = "LbMetryka2";
            this.LbMetryka2.Size = new System.Drawing.Size(61, 18);
            this.LbMetryka2.TabIndex = 46;
            this.LbMetryka2.Text = "Metryka";
            // 
            // LbWynik2
            // 
            this.LbWynik2.AutoSize = true;
            this.LbWynik2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWynik2.Location = new System.Drawing.Point(860, 625);
            this.LbWynik2.Name = "LbWynik2";
            this.LbWynik2.Size = new System.Drawing.Size(49, 18);
            this.LbWynik2.TabIndex = 44;
            this.LbWynik2.Text = "Wynik";
            // 
            // TbWynik2
            // 
            this.TbWynik2.Location = new System.Drawing.Point(917, 626);
            this.TbWynik2.Name = "TbWynik2";
            this.TbWynik2.Size = new System.Drawing.Size(117, 20);
            this.TbWynik2.TabIndex = 43;
            // 
            // TbParametrk2
            // 
            this.TbParametrk2.Location = new System.Drawing.Point(917, 415);
            this.TbParametrk2.Name = "TbParametrk2";
            this.TbParametrk2.Size = new System.Drawing.Size(117, 20);
            this.TbParametrk2.TabIndex = 42;
            this.TbParametrk2.Text = "3";
            // 
            // LbParametrk2
            // 
            this.LbParametrk2.AutoSize = true;
            this.LbParametrk2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbParametrk2.Location = new System.Drawing.Point(830, 415);
            this.LbParametrk2.Name = "LbParametrk2";
            this.LbParametrk2.Size = new System.Drawing.Size(81, 18);
            this.LbParametrk2.TabIndex = 41;
            this.LbParametrk2.Text = "Parametr k";
            // 
            // CbObiekty
            // 
            this.CbObiekty.FormattingEnabled = true;
            this.CbObiekty.Location = new System.Drawing.Point(915, 264);
            this.CbObiekty.Name = "CbObiekty";
            this.CbObiekty.Size = new System.Drawing.Size(115, 21);
            this.CbObiekty.TabIndex = 48;
            // 
            // LbWybierz
            // 
            this.LbWybierz.AutoSize = true;
            this.LbWybierz.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWybierz.Location = new System.Drawing.Point(755, 264);
            this.LbWybierz.Name = "LbWybierz";
            this.LbWybierz.Size = new System.Drawing.Size(154, 18);
            this.LbWybierz.TabIndex = 49;
            this.LbWybierz.Text = "Wybierz baze testowa";
            // 
            // LbStatystyka
            // 
            this.LbStatystyka.AutoSize = true;
            this.LbStatystyka.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbStatystyka.Location = new System.Drawing.Point(644, 676);
            this.LbStatystyka.Name = "LbStatystyka";
            this.LbStatystyka.Size = new System.Drawing.Size(122, 25);
            this.LbStatystyka.TabIndex = 50;
            this.LbStatystyka.Text = "Statystyka";
            // 
            // CbMetryka3
            // 
            this.CbMetryka3.FormattingEnabled = true;
            this.CbMetryka3.Location = new System.Drawing.Point(693, 725);
            this.CbMetryka3.Name = "CbMetryka3";
            this.CbMetryka3.Size = new System.Drawing.Size(115, 21);
            this.CbMetryka3.TabIndex = 51;
            // 
            // LbMetryka3
            // 
            this.LbMetryka3.AutoSize = true;
            this.LbMetryka3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbMetryka3.Location = new System.Drawing.Point(614, 728);
            this.LbMetryka3.Name = "LbMetryka3";
            this.LbMetryka3.Size = new System.Drawing.Size(61, 18);
            this.LbMetryka3.TabIndex = 52;
            this.LbMetryka3.Text = "Metryka";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(831, 687);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(367, 183);
            this.dataGridView3.TabIndex = 53;
            // 
            // LbWariant3
            // 
            this.LbWariant3.AutoSize = true;
            this.LbWariant3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWariant3.Location = new System.Drawing.Point(616, 764);
            this.LbWariant3.Name = "LbWariant3";
            this.LbWariant3.Size = new System.Drawing.Size(59, 18);
            this.LbWariant3.TabIndex = 54;
            this.LbWariant3.Text = "Wariant";
            // 
            // CbWariant3
            // 
            this.CbWariant3.FormattingEnabled = true;
            this.CbWariant3.Location = new System.Drawing.Point(695, 761);
            this.CbWariant3.Name = "CbWariant3";
            this.CbWariant3.Size = new System.Drawing.Size(113, 21);
            this.CbWariant3.TabIndex = 55;
            // 
            // LbParametrk3
            // 
            this.LbParametrk3.AutoSize = true;
            this.LbParametrk3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbParametrk3.Location = new System.Drawing.Point(594, 799);
            this.LbParametrk3.Name = "LbParametrk3";
            this.LbParametrk3.Size = new System.Drawing.Size(81, 18);
            this.LbParametrk3.TabIndex = 56;
            this.LbParametrk3.Text = "Parametr k";
            // 
            // TbParametrk3
            // 
            this.TbParametrk3.Location = new System.Drawing.Point(693, 797);
            this.TbParametrk3.Name = "TbParametrk3";
            this.TbParametrk3.Size = new System.Drawing.Size(115, 20);
            this.TbParametrk3.TabIndex = 57;
            this.TbParametrk3.Text = "3";
            // 
            // BtSkutecznosc
            // 
            this.BtSkutecznosc.Location = new System.Drawing.Point(694, 838);
            this.BtSkutecznosc.Name = "BtSkutecznosc";
            this.BtSkutecznosc.Size = new System.Drawing.Size(114, 32);
            this.BtSkutecznosc.TabIndex = 58;
            this.BtSkutecznosc.Text = "Oblicz skutecznosc";
            this.BtSkutecznosc.UseVisualStyleBackColor = true;
            this.BtSkutecznosc.Click += new System.EventHandler(this.BtSkutecznosc_Click);
            // 
            // BtnUsun
            // 
            this.BtnUsun.Location = new System.Drawing.Point(27, 197);
            this.BtnUsun.Name = "BtnUsun";
            this.BtnUsun.Size = new System.Drawing.Size(114, 32);
            this.BtnUsun.TabIndex = 60;
            this.BtnUsun.Text = "Usuń Atrybut";
            this.BtnUsun.UseVisualStyleBackColor = true;
            this.BtnUsun.Click += new System.EventHandler(this.BtnUsun_Click);
            // 
            // BtnWczytajConfiga
            // 
            this.BtnWczytajConfiga.Location = new System.Drawing.Point(296, 197);
            this.BtnWczytajConfiga.Name = "BtnWczytajConfiga";
            this.BtnWczytajConfiga.Size = new System.Drawing.Size(114, 32);
            this.BtnWczytajConfiga.TabIndex = 61;
            this.BtnWczytajConfiga.Text = "Wczytaj Configa";
            this.BtnWczytajConfiga.UseVisualStyleBackColor = true;
            this.BtnWczytajConfiga.Click += new System.EventHandler(this.BtnWczytajConfiga_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // CbUsun
            // 
            this.CbUsun.FormattingEnabled = true;
            this.CbUsun.Location = new System.Drawing.Point(153, 204);
            this.CbUsun.Name = "CbUsun";
            this.CbUsun.Size = new System.Drawing.Size(121, 21);
            this.CbUsun.TabIndex = 62;
            // 
            // TbKonkretny
            // 
            this.TbKonkretny.Location = new System.Drawing.Point(917, 538);
            this.TbKonkretny.Name = "TbKonkretny";
            this.TbKonkretny.Size = new System.Drawing.Size(117, 20);
            this.TbKonkretny.TabIndex = 63;
            this.TbKonkretny.Text = "6,5-3-5,2-2";
            // 
            // LbKonkretny
            // 
            this.LbKonkretny.AutoSize = true;
            this.LbKonkretny.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbKonkretny.Location = new System.Drawing.Point(685, 540);
            this.LbKonkretny.Name = "LbKonkretny";
            this.LbKonkretny.Size = new System.Drawing.Size(224, 18);
            this.LbKonkretny.TabIndex = 64;
            this.LbKonkretny.Text = "Wybierz konkretna baze testowa";
            // 
            // BtnKonkretnyNor
            // 
            this.BtnKonkretnyNor.Location = new System.Drawing.Point(799, 574);
            this.BtnKonkretnyNor.Name = "BtnKonkretnyNor";
            this.BtnKonkretnyNor.Size = new System.Drawing.Size(170, 32);
            this.BtnKonkretnyNor.TabIndex = 65;
            this.BtnKonkretnyNor.Text = "Klasyfikuj obiekt znormalizowany";
            this.BtnKonkretnyNor.UseVisualStyleBackColor = true;
            this.BtnKonkretnyNor.Click += new System.EventHandler(this.BtnKonkretnyNor_Click);
            // 
            // BtnKonkretnyNieNor
            // 
            this.BtnKonkretnyNieNor.Location = new System.Drawing.Point(984, 574);
            this.BtnKonkretnyNieNor.Name = "BtnKonkretnyNieNor";
            this.BtnKonkretnyNieNor.Size = new System.Drawing.Size(186, 32);
            this.BtnKonkretnyNieNor.TabIndex = 66;
            this.BtnKonkretnyNieNor.Text = "Klasyfikuj obiekt nieznormalizowany";
            this.BtnKonkretnyNieNor.UseVisualStyleBackColor = true;
            this.BtnKonkretnyNieNor.Click += new System.EventHandler(this.BtnKonkretnyNieNor_Click);
            // 
            // CbWariant
            // 
            this.CbWariant.FormattingEnabled = true;
            this.CbWariant.Location = new System.Drawing.Point(915, 225);
            this.CbWariant.Name = "CbWariant";
            this.CbWariant.Size = new System.Drawing.Size(115, 21);
            this.CbWariant.TabIndex = 68;
            // 
            // LbWariant
            // 
            this.LbWariant.AutoSize = true;
            this.LbWariant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWariant.Location = new System.Drawing.Point(850, 225);
            this.LbWariant.Name = "LbWariant";
            this.LbWariant.Size = new System.Drawing.Size(59, 18);
            this.LbWariant.TabIndex = 67;
            this.LbWariant.Text = "Wariant";
            // 
            // CbWariant2
            // 
            this.CbWariant2.FormattingEnabled = true;
            this.CbWariant2.Location = new System.Drawing.Point(919, 498);
            this.CbWariant2.Name = "CbWariant2";
            this.CbWariant2.Size = new System.Drawing.Size(115, 21);
            this.CbWariant2.TabIndex = 70;
            // 
            // LbWariant2
            // 
            this.LbWariant2.AutoSize = true;
            this.LbWariant2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWariant2.Location = new System.Drawing.Point(854, 501);
            this.LbWariant2.Name = "LbWariant2";
            this.LbWariant2.Size = new System.Drawing.Size(59, 18);
            this.LbWariant2.TabIndex = 69;
            this.LbWariant2.Text = "Wariant";
            // 
            // LbWszystkie
            // 
            this.LbWszystkie.AutoSize = true;
            this.LbWszystkie.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWszystkie.Location = new System.Drawing.Point(148, 532);
            this.LbWszystkie.Name = "LbWszystkie";
            this.LbWszystkie.Size = new System.Drawing.Size(233, 25);
            this.LbWszystkie.TabIndex = 71;
            this.LbWszystkie.Text = "Wszystkie odległości";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 916);
            this.Controls.Add(this.LbWszystkie);
            this.Controls.Add(this.CbWariant2);
            this.Controls.Add(this.LbWariant2);
            this.Controls.Add(this.CbWariant);
            this.Controls.Add(this.LbWariant);
            this.Controls.Add(this.BtnKonkretnyNieNor);
            this.Controls.Add(this.BtnKonkretnyNor);
            this.Controls.Add(this.LbKonkretny);
            this.Controls.Add(this.TbKonkretny);
            this.Controls.Add(this.CbUsun);
            this.Controls.Add(this.BtnWczytajConfiga);
            this.Controls.Add(this.BtnUsun);
            this.Controls.Add(this.BtSkutecznosc);
            this.Controls.Add(this.TbParametrk3);
            this.Controls.Add(this.LbParametrk3);
            this.Controls.Add(this.CbWariant3);
            this.Controls.Add(this.LbWariant3);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.LbMetryka3);
            this.Controls.Add(this.CbMetryka3);
            this.Controls.Add(this.LbStatystyka);
            this.Controls.Add(this.LbWybierz);
            this.Controls.Add(this.CbObiekty);
            this.Controls.Add(this.CbMetryka2);
            this.Controls.Add(this.LbMetryka2);
            this.Controls.Add(this.LbWynik2);
            this.Controls.Add(this.TbWynik2);
            this.Controls.Add(this.TbParametrk2);
            this.Controls.Add(this.LbParametrk2);
            this.Controls.Add(this.CbMetryka);
            this.Controls.Add(this.LbMetryka);
            this.Controls.Add(this.BtKlasyfikuj);
            this.Controls.Add(this.LbWynik);
            this.Controls.Add(this.TbWynik);
            this.Controls.Add(this.TbParametrk);
            this.Controls.Add(this.LbParametrk);
            this.Controls.Add(this.LbProbkaWlasna);
            this.Controls.Add(this.LbProbkaBaza);
            this.Controls.Add(this.BtOdleglosci);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.CbLista);
            this.Controls.Add(this.BtnDecyzyjna);
            this.Controls.Add(this.TbZakres2);
            this.Controls.Add(this.TbZakres1);
            this.Controls.Add(this.LbZakresNormalizacji);
            this.Controls.Add(this.BtnNormalizuj);
            this.Controls.Add(this.BtnWypisz);
            this.Controls.Add(this.LbTytul);
            this.Controls.Add(this.LbWczytaj);
            this.Controls.Add(this.BtnWczytaj);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbWczytaj;
        private System.Windows.Forms.Button BtnWczytaj;
        private System.Windows.Forms.Label LbTytul;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox CbLista;
        private System.Windows.Forms.Button BtnDecyzyjna;
        private System.Windows.Forms.TextBox TbZakres2;
        private System.Windows.Forms.TextBox TbZakres1;
        private System.Windows.Forms.Label LbZakresNormalizacji;
        private System.Windows.Forms.Button BtnNormalizuj;
        private System.Windows.Forms.Button BtnWypisz;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button BtOdleglosci;
        private System.Windows.Forms.Label LbProbkaBaza;
        private System.Windows.Forms.Label LbProbkaWlasna;
        private System.Windows.Forms.TextBox TbParametrk;
        private System.Windows.Forms.Label LbParametrk;
        private System.Windows.Forms.TextBox TbWynik;
        private System.Windows.Forms.Label LbWynik;
        private System.Windows.Forms.Button BtKlasyfikuj;
        private System.Windows.Forms.Label LbMetryka;
        private System.Windows.Forms.ComboBox CbMetryka;
        private System.Windows.Forms.ComboBox CbMetryka2;
        private System.Windows.Forms.Label LbMetryka2;
        private System.Windows.Forms.Label LbWynik2;
        private System.Windows.Forms.TextBox TbWynik2;
        private System.Windows.Forms.TextBox TbParametrk2;
        private System.Windows.Forms.Label LbParametrk2;
        private System.Windows.Forms.ComboBox CbObiekty;
        private System.Windows.Forms.Label LbWybierz;
        private System.Windows.Forms.Label LbStatystyka;
        private System.Windows.Forms.ComboBox CbMetryka3;
        private System.Windows.Forms.Label LbMetryka3;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label LbWariant3;
        private System.Windows.Forms.ComboBox CbWariant3;
        private System.Windows.Forms.Label LbParametrk3;
        private System.Windows.Forms.TextBox TbParametrk3;
        private System.Windows.Forms.Button BtSkutecznosc;
        private System.Windows.Forms.Button BtnUsun;
        private System.Windows.Forms.Button BtnWczytajConfiga;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.ComboBox CbUsun;
        private System.Windows.Forms.TextBox TbKonkretny;
        private System.Windows.Forms.Label LbKonkretny;
        private System.Windows.Forms.Button BtnKonkretnyNor;
        private System.Windows.Forms.Button BtnKonkretnyNieNor;
        private System.Windows.Forms.ComboBox CbWariant;
        private System.Windows.Forms.Label LbWariant;
        private System.Windows.Forms.ComboBox CbWariant2;
        private System.Windows.Forms.Label LbWariant2;
        private System.Windows.Forms.Label LbWszystkie;
    }
}

