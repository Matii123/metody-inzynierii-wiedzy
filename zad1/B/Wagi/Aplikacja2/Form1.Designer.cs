﻿
namespace Aplikacja2
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbTytyl = new System.Windows.Forms.Label();
            this.TbStruktura = new System.Windows.Forms.TextBox();
            this.TbPoczatek = new System.Windows.Forms.TextBox();
            this.TbKoniec = new System.Windows.Forms.TextBox();
            this.LbStruktura = new System.Windows.Forms.Label();
            this.LbPoczatek = new System.Windows.Forms.Label();
            this.LbKoniec = new System.Windows.Forms.Label();
            this.BtGeneruj = new System.Windows.Forms.Button();
            this.RtbWynik = new System.Windows.Forms.RichTextBox();
            this.BtWczytaj = new System.Windows.Forms.Button();
            this.BtZapisz = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // LbTytyl
            // 
            this.LbTytyl.AutoSize = true;
            this.LbTytyl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbTytyl.Location = new System.Drawing.Point(316, 32);
            this.LbTytyl.Name = "LbTytyl";
            this.LbTytyl.Size = new System.Drawing.Size(177, 24);
            this.LbTytyl.TabIndex = 0;
            this.LbTytyl.Text = "Projekt 1 Część: 2";
            // 
            // TbStruktura
            // 
            this.TbStruktura.Location = new System.Drawing.Point(164, 127);
            this.TbStruktura.Name = "TbStruktura";
            this.TbStruktura.Size = new System.Drawing.Size(136, 20);
            this.TbStruktura.TabIndex = 1;
            this.TbStruktura.Text = "3-2-2";
            // 
            // TbPoczatek
            // 
            this.TbPoczatek.Location = new System.Drawing.Point(164, 181);
            this.TbPoczatek.Name = "TbPoczatek";
            this.TbPoczatek.Size = new System.Drawing.Size(136, 20);
            this.TbPoczatek.TabIndex = 2;
            this.TbPoczatek.Text = "0";
            // 
            // TbKoniec
            // 
            this.TbKoniec.Location = new System.Drawing.Point(164, 238);
            this.TbKoniec.Name = "TbKoniec";
            this.TbKoniec.Size = new System.Drawing.Size(136, 20);
            this.TbKoniec.TabIndex = 3;
            this.TbKoniec.Text = "1";
            // 
            // LbStruktura
            // 
            this.LbStruktura.AutoSize = true;
            this.LbStruktura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbStruktura.Location = new System.Drawing.Point(12, 131);
            this.LbStruktura.Name = "LbStruktura";
            this.LbStruktura.Size = new System.Drawing.Size(69, 16);
            this.LbStruktura.TabIndex = 4;
            this.LbStruktura.Text = "Struktura";
            // 
            // LbPoczatek
            // 
            this.LbPoczatek.AutoSize = true;
            this.LbPoczatek.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbPoczatek.Location = new System.Drawing.Point(12, 185);
            this.LbPoczatek.Name = "LbPoczatek";
            this.LbPoczatek.Size = new System.Drawing.Size(130, 16);
            this.LbPoczatek.TabIndex = 5;
            this.LbPoczatek.Text = "Początek zakresu";
            // 
            // LbKoniec
            // 
            this.LbKoniec.AutoSize = true;
            this.LbKoniec.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbKoniec.Location = new System.Drawing.Point(12, 242);
            this.LbKoniec.Name = "LbKoniec";
            this.LbKoniec.Size = new System.Drawing.Size(113, 16);
            this.LbKoniec.TabIndex = 6;
            this.LbKoniec.Text = "Koniec zakresu";
            // 
            // BtGeneruj
            // 
            this.BtGeneruj.Location = new System.Drawing.Point(194, 279);
            this.BtGeneruj.Name = "BtGeneruj";
            this.BtGeneruj.Size = new System.Drawing.Size(106, 23);
            this.BtGeneruj.TabIndex = 7;
            this.BtGeneruj.Text = "Generuj dane";
            this.BtGeneruj.UseVisualStyleBackColor = true;
            this.BtGeneruj.Click += new System.EventHandler(this.BtGeneruj_Click);
            // 
            // RtbWynik
            // 
            this.RtbWynik.Location = new System.Drawing.Point(404, 127);
            this.RtbWynik.Name = "RtbWynik";
            this.RtbWynik.Size = new System.Drawing.Size(359, 131);
            this.RtbWynik.TabIndex = 9;
            this.RtbWynik.Text = "";
            // 
            // BtWczytaj
            // 
            this.BtWczytaj.Location = new System.Drawing.Point(404, 279);
            this.BtWczytaj.Name = "BtWczytaj";
            this.BtWczytaj.Size = new System.Drawing.Size(106, 23);
            this.BtWczytaj.TabIndex = 10;
            this.BtWczytaj.Text = "Wczytaj dane";
            this.BtWczytaj.UseVisualStyleBackColor = true;
            this.BtWczytaj.Click += new System.EventHandler(this.BtWczytaj_Click);
            // 
            // BtZapisz
            // 
            this.BtZapisz.Location = new System.Drawing.Point(657, 279);
            this.BtZapisz.Name = "BtZapisz";
            this.BtZapisz.Size = new System.Drawing.Size(106, 23);
            this.BtZapisz.TabIndex = 11;
            this.BtZapisz.Text = "Zapisz dane";
            this.BtZapisz.UseVisualStyleBackColor = true;
            this.BtZapisz.Click += new System.EventHandler(this.BtZapisz_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtZapisz);
            this.Controls.Add(this.BtWczytaj);
            this.Controls.Add(this.RtbWynik);
            this.Controls.Add(this.BtGeneruj);
            this.Controls.Add(this.LbKoniec);
            this.Controls.Add(this.LbPoczatek);
            this.Controls.Add(this.LbStruktura);
            this.Controls.Add(this.TbKoniec);
            this.Controls.Add(this.TbPoczatek);
            this.Controls.Add(this.TbStruktura);
            this.Controls.Add(this.LbTytyl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbTytyl;
        private System.Windows.Forms.TextBox TbStruktura;
        private System.Windows.Forms.TextBox TbPoczatek;
        private System.Windows.Forms.TextBox TbKoniec;
        private System.Windows.Forms.Label LbStruktura;
        private System.Windows.Forms.Label LbPoczatek;
        private System.Windows.Forms.Label LbKoniec;
        private System.Windows.Forms.Button BtGeneruj;
        private System.Windows.Forms.RichTextBox RtbWynik;
        private System.Windows.Forms.Button BtWczytaj;
        private System.Windows.Forms.Button BtZapisz;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

