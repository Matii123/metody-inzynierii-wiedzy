﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplikacja2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtGeneruj_Click(object sender, EventArgs e)
        {
            char sperarator = '-';
            string[] tekst = (TbStruktura.Text).Split(sperarator);
            int[] liczby = new int[tekst.Length];

            for(int i = 0; i < liczby.Length; i++)
            {
                liczby[i] = int.Parse(tekst[i]);
            }

            double pocz = double.Parse(TbPoczatek.Text);
            double koniec = double.Parse(TbKoniec.Text);

            string wynik = Generuj(liczby, pocz, koniec);

            RtbWynik.Text = wynik;
        }
        private void BtWczytaj_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string sciezka = openFileDialog1.FileName;
            string[] lines = File.ReadAllLines(sciezka);
            string napis = "";
            for (int i = 0; i < lines.Length; i++)
            {
                napis = napis + lines[i] + "\n";
            }
            RtbWynik.Text = napis;
        }

        private void BtZapisz_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter(@".\plik.txt");
            sw.Write(RtbWynik.Text);
            sw.Close();
        }
        string Generuj(int[] liczby, double pocz, double koniec)
        {
            Random generator = new Random();
            string wynik = "";
            for(int i = 1; i < liczby.Length; i++)
            {
                double[,] tablica = new double[liczby[i], liczby[i-1] + 1];
                for(int x = 0; x < tablica.GetLength(0); x++)
                {
                    for (int y = 0; y < tablica.GetLength(1); y++)
                    {
                        //przypisanie liczby o odpowiednim przedziale do tablicy
                        tablica[x, y] = Math.Round(pocz + generator.NextDouble()* (koniec-pocz),3);
                        wynik = wynik + tablica[x,y] + " ";
                    }
                    wynik = wynik + "\n";
                }
                wynik = wynik + "\n";
            }
            return wynik;
        }
    }
}
