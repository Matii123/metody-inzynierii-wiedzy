﻿
namespace Aplikacja
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.BtnWczytaj = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.LbWczytaj = new System.Windows.Forms.Label();
            this.BtnWypisz = new System.Windows.Forms.Button();
            this.BtnNormalizuj = new System.Windows.Forms.Button();
            this.LbTytul = new System.Windows.Forms.Label();
            this.LbFormat = new System.Windows.Forms.Label();
            this.RbCSV = new System.Windows.Forms.RadioButton();
            this.RbXML = new System.Windows.Forms.RadioButton();
            this.RbJSON = new System.Windows.Forms.RadioButton();
            this.BtZapisz = new System.Windows.Forms.Button();
            this.LbZakresNormalizacji = new System.Windows.Forms.Label();
            this.TbZakres1 = new System.Windows.Forms.TextBox();
            this.TbZakres2 = new System.Windows.Forms.TextBox();
            this.RtbKonfiguracyjny = new System.Windows.Forms.RichTextBox();
            this.BtnGeneruj = new System.Windows.Forms.Button();
            this.BtnDecyzyjna = new System.Windows.Forms.Button();
            this.CbLista = new System.Windows.Forms.ComboBox();
            this.BtnWczytajConfiga = new System.Windows.Forms.Button();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.BtnUsun = new System.Windows.Forms.Button();
            this.CbUsun = new System.Windows.Forms.ComboBox();
            this.CblNormalizuj = new System.Windows.Forms.CheckedListBox();
            this.LbAtrybuty = new System.Windows.Forms.Label();
            this.BtnSprawdzDane = new System.Windows.Forms.Button();
            this.RtbSprawdzenie = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(31, 280);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1140, 238);
            this.dataGridView1.TabIndex = 0;
            // 
            // BtnWczytaj
            // 
            this.BtnWczytaj.Location = new System.Drawing.Point(31, 66);
            this.BtnWczytaj.Name = "BtnWczytaj";
            this.BtnWczytaj.Size = new System.Drawing.Size(114, 32);
            this.BtnWczytaj.TabIndex = 1;
            this.BtnWczytaj.Text = "Wczytaj plik";
            this.BtnWczytaj.UseVisualStyleBackColor = true;
            this.BtnWczytaj.Click += new System.EventHandler(this.BtnWczytaj_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // LbWczytaj
            // 
            this.LbWczytaj.AutoSize = true;
            this.LbWczytaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbWczytaj.Location = new System.Drawing.Point(154, 72);
            this.LbWczytaj.Name = "LbWczytaj";
            this.LbWczytaj.Size = new System.Drawing.Size(39, 18);
            this.LbWczytaj.TabIndex = 2;
            this.LbWczytaj.Text = "Brak";
            // 
            // BtnWypisz
            // 
            this.BtnWypisz.Location = new System.Drawing.Point(31, 116);
            this.BtnWypisz.Name = "BtnWypisz";
            this.BtnWypisz.Size = new System.Drawing.Size(114, 32);
            this.BtnWypisz.TabIndex = 3;
            this.BtnWypisz.Text = "Wypisz dane";
            this.BtnWypisz.UseVisualStyleBackColor = true;
            this.BtnWypisz.Click += new System.EventHandler(this.BtnWypisz_Click);
            // 
            // BtnNormalizuj
            // 
            this.BtnNormalizuj.Location = new System.Drawing.Point(136, 630);
            this.BtnNormalizuj.Name = "BtnNormalizuj";
            this.BtnNormalizuj.Size = new System.Drawing.Size(114, 32);
            this.BtnNormalizuj.TabIndex = 5;
            this.BtnNormalizuj.Text = "Normalizuj dane";
            this.BtnNormalizuj.UseVisualStyleBackColor = true;
            this.BtnNormalizuj.Click += new System.EventHandler(this.BtnNormalizuj_Click);
            // 
            // LbTytul
            // 
            this.LbTytul.AutoSize = true;
            this.LbTytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbTytul.Location = new System.Drawing.Point(530, 18);
            this.LbTytul.Name = "LbTytul";
            this.LbTytul.Size = new System.Drawing.Size(106, 25);
            this.LbTytul.TabIndex = 6;
            this.LbTytul.Text = "Projekt 1";
            // 
            // LbFormat
            // 
            this.LbFormat.AutoSize = true;
            this.LbFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbFormat.Location = new System.Drawing.Point(747, 543);
            this.LbFormat.Name = "LbFormat";
            this.LbFormat.Size = new System.Drawing.Size(207, 18);
            this.LbFormat.TabIndex = 7;
            this.LbFormat.Text = "Wybierz format zapisu danych";
            // 
            // RbCSV
            // 
            this.RbCSV.AutoSize = true;
            this.RbCSV.Location = new System.Drawing.Point(981, 545);
            this.RbCSV.Name = "RbCSV";
            this.RbCSV.Size = new System.Drawing.Size(46, 17);
            this.RbCSV.TabIndex = 8;
            this.RbCSV.TabStop = true;
            this.RbCSV.Text = "CSV";
            this.RbCSV.UseVisualStyleBackColor = true;
            // 
            // RbXML
            // 
            this.RbXML.AutoSize = true;
            this.RbXML.Location = new System.Drawing.Point(1054, 545);
            this.RbXML.Name = "RbXML";
            this.RbXML.Size = new System.Drawing.Size(47, 17);
            this.RbXML.TabIndex = 9;
            this.RbXML.TabStop = true;
            this.RbXML.Text = "XML";
            this.RbXML.UseVisualStyleBackColor = true;
            // 
            // RbJSON
            // 
            this.RbJSON.AutoSize = true;
            this.RbJSON.Location = new System.Drawing.Point(1119, 545);
            this.RbJSON.Name = "RbJSON";
            this.RbJSON.Size = new System.Drawing.Size(53, 17);
            this.RbJSON.TabIndex = 10;
            this.RbJSON.TabStop = true;
            this.RbJSON.Text = "JSON";
            this.RbJSON.UseVisualStyleBackColor = true;
            // 
            // BtZapisz
            // 
            this.BtZapisz.Location = new System.Drawing.Point(750, 574);
            this.BtZapisz.Name = "BtZapisz";
            this.BtZapisz.Size = new System.Drawing.Size(114, 32);
            this.BtZapisz.TabIndex = 11;
            this.BtZapisz.Text = "Zapisz Dane";
            this.BtZapisz.UseVisualStyleBackColor = true;
            this.BtZapisz.Click += new System.EventHandler(this.BtZapisz_Click);
            // 
            // LbZakresNormalizacji
            // 
            this.LbZakresNormalizacji.AutoSize = true;
            this.LbZakresNormalizacji.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbZakresNormalizacji.Location = new System.Drawing.Point(28, 543);
            this.LbZakresNormalizacji.Name = "LbZakresNormalizacji";
            this.LbZakresNormalizacji.Size = new System.Drawing.Size(194, 18);
            this.LbZakresNormalizacji.TabIndex = 12;
            this.LbZakresNormalizacji.Text = "Wybierz zakres normalizacji";
            // 
            // TbZakres1
            // 
            this.TbZakres1.Location = new System.Drawing.Point(225, 544);
            this.TbZakres1.Name = "TbZakres1";
            this.TbZakres1.Size = new System.Drawing.Size(68, 20);
            this.TbZakres1.TabIndex = 13;
            this.TbZakres1.Text = "0";
            // 
            // TbZakres2
            // 
            this.TbZakres2.Location = new System.Drawing.Point(308, 544);
            this.TbZakres2.Name = "TbZakres2";
            this.TbZakres2.Size = new System.Drawing.Size(68, 20);
            this.TbZakres2.TabIndex = 14;
            this.TbZakres2.Text = "1";
            // 
            // RtbKonfiguracyjny
            // 
            this.RtbKonfiguracyjny.Location = new System.Drawing.Point(799, 90);
            this.RtbKonfiguracyjny.Name = "RtbKonfiguracyjny";
            this.RtbKonfiguracyjny.Size = new System.Drawing.Size(372, 99);
            this.RtbKonfiguracyjny.TabIndex = 15;
            this.RtbKonfiguracyjny.Text = "";
            // 
            // BtnGeneruj
            // 
            this.BtnGeneruj.Location = new System.Drawing.Point(679, 114);
            this.BtnGeneruj.Name = "BtnGeneruj";
            this.BtnGeneruj.Size = new System.Drawing.Size(114, 32);
            this.BtnGeneruj.TabIndex = 16;
            this.BtnGeneruj.Text = "Generuj Configa";
            this.BtnGeneruj.UseVisualStyleBackColor = true;
            this.BtnGeneruj.Click += new System.EventHandler(this.BtnGeneruj_Click);
            // 
            // BtnDecyzyjna
            // 
            this.BtnDecyzyjna.Location = new System.Drawing.Point(157, 116);
            this.BtnDecyzyjna.Name = "BtnDecyzyjna";
            this.BtnDecyzyjna.Size = new System.Drawing.Size(151, 32);
            this.BtnDecyzyjna.TabIndex = 17;
            this.BtnDecyzyjna.Text = "Wybierz klase decyzyjną";
            this.BtnDecyzyjna.UseVisualStyleBackColor = true;
            this.BtnDecyzyjna.Click += new System.EventHandler(this.BtnDecyzyjna_Click);
            // 
            // CbLista
            // 
            this.CbLista.FormattingEnabled = true;
            this.CbLista.Location = new System.Drawing.Point(314, 123);
            this.CbLista.Name = "CbLista";
            this.CbLista.Size = new System.Drawing.Size(121, 21);
            this.CbLista.TabIndex = 18;
            // 
            // BtnWczytajConfiga
            // 
            this.BtnWczytajConfiga.Location = new System.Drawing.Point(679, 157);
            this.BtnWczytajConfiga.Name = "BtnWczytajConfiga";
            this.BtnWczytajConfiga.Size = new System.Drawing.Size(114, 32);
            this.BtnWczytajConfiga.TabIndex = 19;
            this.BtnWczytajConfiga.Text = "Wczytaj Configa";
            this.BtnWczytajConfiga.UseVisualStyleBackColor = true;
            this.BtnWczytajConfiga.Click += new System.EventHandler(this.BtnWczytajConfiga_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // BtnUsun
            // 
            this.BtnUsun.Location = new System.Drawing.Point(31, 165);
            this.BtnUsun.Name = "BtnUsun";
            this.BtnUsun.Size = new System.Drawing.Size(114, 32);
            this.BtnUsun.TabIndex = 20;
            this.BtnUsun.Text = "Usuń atrybut";
            this.BtnUsun.UseVisualStyleBackColor = true;
            this.BtnUsun.Click += new System.EventHandler(this.BtnUsun_Click);
            // 
            // CbUsun
            // 
            this.CbUsun.FormattingEnabled = true;
            this.CbUsun.Location = new System.Drawing.Point(151, 172);
            this.CbUsun.Name = "CbUsun";
            this.CbUsun.Size = new System.Drawing.Size(121, 21);
            this.CbUsun.TabIndex = 21;
            // 
            // CblNormalizuj
            // 
            this.CblNormalizuj.FormattingEnabled = true;
            this.CblNormalizuj.Location = new System.Drawing.Point(256, 583);
            this.CblNormalizuj.Name = "CblNormalizuj";
            this.CblNormalizuj.Size = new System.Drawing.Size(120, 79);
            this.CblNormalizuj.TabIndex = 22;
            // 
            // LbAtrybuty
            // 
            this.LbAtrybuty.AutoSize = true;
            this.LbAtrybuty.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LbAtrybuty.Location = new System.Drawing.Point(28, 583);
            this.LbAtrybuty.Name = "LbAtrybuty";
            this.LbAtrybuty.Size = new System.Drawing.Size(222, 18);
            this.LbAtrybuty.TabIndex = 23;
            this.LbAtrybuty.Text = "Wybierz atrybuty normalizowane";
            // 
            // BtnSprawdzDane
            // 
            this.BtnSprawdzDane.Location = new System.Drawing.Point(679, 241);
            this.BtnSprawdzDane.Name = "BtnSprawdzDane";
            this.BtnSprawdzDane.Size = new System.Drawing.Size(114, 32);
            this.BtnSprawdzDane.TabIndex = 24;
            this.BtnSprawdzDane.Text = "Sprawdz dane";
            this.BtnSprawdzDane.UseVisualStyleBackColor = true;
            this.BtnSprawdzDane.Click += new System.EventHandler(this.BtnSprawdzDane_Click);
            // 
            // RtbSprawdzenie
            // 
            this.RtbSprawdzenie.Location = new System.Drawing.Point(799, 195);
            this.RtbSprawdzenie.Name = "RtbSprawdzenie";
            this.RtbSprawdzenie.Size = new System.Drawing.Size(372, 78);
            this.RtbSprawdzenie.TabIndex = 25;
            this.RtbSprawdzenie.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1186, 680);
            this.Controls.Add(this.RtbSprawdzenie);
            this.Controls.Add(this.BtnSprawdzDane);
            this.Controls.Add(this.LbAtrybuty);
            this.Controls.Add(this.CblNormalizuj);
            this.Controls.Add(this.CbUsun);
            this.Controls.Add(this.BtnUsun);
            this.Controls.Add(this.BtnWczytajConfiga);
            this.Controls.Add(this.CbLista);
            this.Controls.Add(this.BtnDecyzyjna);
            this.Controls.Add(this.BtnGeneruj);
            this.Controls.Add(this.RtbKonfiguracyjny);
            this.Controls.Add(this.TbZakres2);
            this.Controls.Add(this.TbZakres1);
            this.Controls.Add(this.LbZakresNormalizacji);
            this.Controls.Add(this.BtZapisz);
            this.Controls.Add(this.RbJSON);
            this.Controls.Add(this.RbXML);
            this.Controls.Add(this.RbCSV);
            this.Controls.Add(this.LbFormat);
            this.Controls.Add(this.LbTytul);
            this.Controls.Add(this.BtnNormalizuj);
            this.Controls.Add(this.BtnWypisz);
            this.Controls.Add(this.LbWczytaj);
            this.Controls.Add(this.BtnWczytaj);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button BtnWczytaj;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label LbWczytaj;
        private System.Windows.Forms.Button BtnWypisz;
        private System.Windows.Forms.Button BtnNormalizuj;
        private System.Windows.Forms.Label LbTytul;
        private System.Windows.Forms.Label LbFormat;
        private System.Windows.Forms.RadioButton RbCSV;
        private System.Windows.Forms.RadioButton RbXML;
        private System.Windows.Forms.RadioButton RbJSON;
        private System.Windows.Forms.Button BtZapisz;
        private System.Windows.Forms.Label LbZakresNormalizacji;
        private System.Windows.Forms.TextBox TbZakres1;
        private System.Windows.Forms.TextBox TbZakres2;
        private System.Windows.Forms.RichTextBox RtbKonfiguracyjny;
        private System.Windows.Forms.Button BtnGeneruj;
        private System.Windows.Forms.Button BtnDecyzyjna;
        private System.Windows.Forms.ComboBox CbLista;
        private System.Windows.Forms.Button BtnWczytajConfiga;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Button BtnUsun;
        private System.Windows.Forms.ComboBox CbUsun;
        private System.Windows.Forms.CheckedListBox CblNormalizuj;
        private System.Windows.Forms.Label LbAtrybuty;
        private System.Windows.Forms.Button BtnSprawdzDane;
        private System.Windows.Forms.RichTextBox RtbSprawdzenie;
    }
}

