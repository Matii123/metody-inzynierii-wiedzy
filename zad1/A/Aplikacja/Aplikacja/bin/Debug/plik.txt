PLIK KONFIGURACYJNY
Ilosc atrybutów
16
Klasa decyzyjna
A3
Przedział normalizacji
0 1
Kolumny normalizowane
A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12 A13 A14 A15 A16 


INFORMACJE O DANYCH
Atrybut(nazwa, typ, maks, mini, przedzial)
A1 Nienumeryczny  b => 1 a => 2
A2 Numeryczny 13,75 76,75 63
Klasa_decyzyjna Numeryczny 0 28 28
A4 Nienumeryczny  u => 1 y => 2 l => 3
A5 Nienumeryczny  g => 1 p => 2 gg => 3
A6 Nienumeryczny  w => 1 q => 2 m => 3 r => 4 cc => 5 k => 6 c => 7 d => 8 x => 9 i => 10 e => 11 aa => 12 ff => 13 j => 14
A7 Nienumeryczny  v => 1 h => 2 bb => 3 ff => 4 j => 5 z => 6 o => 7 dd => 8 n => 9
A8 Numeryczny 0 28,5 28,5
A9 Nienumeryczny  t => 1 f => 2
A10 Nienumeryczny  t => 1 f => 2
A11 Numeryczny 0 67 67
A12 Nienumeryczny  f => 1 t => 2
A13 Nienumeryczny  g => 1 s => 2 p => 3
A14 Numeryczny 0 2000 2000
A15 Numeryczny 0 100000 100000
A16 Nienumeryczny  + => 1 - => 2
