﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Xml;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace Aplikacja
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string sciezka;
        string sciezka2;
        string[,] tablica_dwuwymiarowa;
        double[,] tablica_normalizowana;
        List<string> nazwy_kolumn = new List<string>();
        List<string> usuniete_kolumny = new List<string>();

        //zmienna wskazujaca klase decyzyjna
        int decyzja = 0;
        string str_decyzja = "A1";

        private void BtnWczytaj_Click(object sender, EventArgs e)
        {
            nazwy_kolumn.Clear();
            usuniete_kolumny.Clear();
            openFileDialog1.ShowDialog();
            sciezka = openFileDialog1.FileName;
            LbWczytaj.Text = sciezka;

            try
            {
                if (sciezka.Contains(".xml"))
                {
                    tablica_dwuwymiarowa = odczytXML();
                    WypiszDane2(tablica_dwuwymiarowa);
                }
                else if(sciezka.Contains(".csv"))
                {
                    tablica_dwuwymiarowa = odczytCSV();
                    WypiszDane2(tablica_dwuwymiarowa);
                }
                else if(sciezka.Contains(".json"))
                {
                    tablica_dwuwymiarowa = odczytJson();
                    WypiszDane2(tablica_dwuwymiarowa);
                }
                else
                {
                    tablica_dwuwymiarowa = WczytajDane(sciezka);
                }

            }
            catch
            {
                LbWczytaj.Text = "Brak";
                MessageBox.Show("Inne rozszerzenie");
            }

        }

        private void BtnWypisz_Click(object sender, EventArgs e)
        {
            //Sprawdzenie czy plik został wczytany
            if (LbWczytaj.Text == "Brak")
            {
                MessageBox.Show("Wybierz plik, który chcesz wczytać");
            }
            else
            {
                WypiszDane(tablica_dwuwymiarowa);
            }
        }
        private void BtnDecyzyjna_Click(object sender, EventArgs e)
        {
            if (CbLista.SelectedItem == null)
            {
                MessageBox.Show("Wybierz klase decyzyjna lub wypisz dane");
            }
            else
            {
                dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.White;
                decyzja = CbLista.SelectedIndex;
                str_decyzja = CbLista.SelectedItem.ToString();

                WypiszDane(tablica_dwuwymiarowa);
            }
        }
        private void BtnNormalizuj_Click(object sender, EventArgs e)
        {
            //Sprawdzenie czy plik został wczytany
            if (LbWczytaj.Text == "Brak")
            {
                MessageBox.Show("Wybierz plik, który chcesz wczytać");
            }
            else
            {
                double poczZakresu = 0;
                double konZakresu = 1;
                try
                {
                    //zmienne do zakresu normalizacji
                    poczZakresu = double.Parse(TbZakres1.Text);
                    konZakresu = double.Parse(TbZakres2.Text);
                }
                catch
                {
                    MessageBox.Show("Zły format danych");
                }

                try
                {
                    List<int> atrybuty = new List<int>();
                    List<string> naglowki = new List<string>();
                    string przesuniecie = dataGridView1.Columns[decyzja].HeaderText;

                    //sprawdzenie ktore atrybuty zostaly zaznaczone
                    for (int i = 0; i < tablica_dwuwymiarowa.GetLength(1); i++)
                    {
                        if (CblNormalizuj.GetItemChecked(i))
                        {
                            atrybuty.Add(i);
                            naglowki.Add(dataGridView1.Columns[i].HeaderText);
                        }
                    }

                    string[,] tablica_pomniejszona;

                    tablica_pomniejszona = WybierzDoNormalizacji(tablica_dwuwymiarowa, atrybuty);
                    NormalizujDane(tablica_pomniejszona, poczZakresu, konZakresu);

                    string[,] tablica_normalizowana_str = new string[tablica_normalizowana.GetLength(0), tablica_normalizowana.GetLength(1)];

                    for (int i = 0; i < tablica_normalizowana.GetLength(0); i++)
                    {
                        for (int j = 0; j < tablica_normalizowana.GetLength(1); j++)
                        {
                            tablica_normalizowana_str[i, j] = tablica_normalizowana[i, j].ToString();
                        }
                    }
                    WypiszDane(tablica_normalizowana_str);
                    CblNormalizuj.Items.Clear();

                    for (int i = 0; i < tablica_normalizowana.GetLength(1); i++)
                    {
                        dataGridView1.Columns[i].HeaderText = naglowki[i];
                        CblNormalizuj.Items.Add(naglowki[i]);
                        CblNormalizuj.SetItemChecked(i,true);
                    }

                    dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
                }
                catch
                {
                    MessageBox.Show("Wybierz plik ponownie lub wypisz dane");
                }
            }
        }
        private void BtZapisz_Click(object sender, EventArgs e)
        {

            //Sprawdzenie czy plik został wczytany
            if (LbWczytaj.Text == "Brak")
            {
                MessageBox.Show("Wybierz plik, który chcesz wczytać");
            }
            else
            {
                if (RbCSV.Checked)
                {
                    ZapiszCSV();

                }
                else if (RbXML.Checked)
                {
                    ZapiszXML();
                }
                else if (RbJSON.Checked)
                {
                    ZapiszJSON();

                }
                else
                {
                    MessageBox.Show("Nie wybrales formatu");
                }
            }
        }
        private void BtnGeneruj_Click(object sender, EventArgs e)
        {
            try
            {
                //Sprawdzenie czy plik został wczytany
                if (LbWczytaj.Text == "Brak")
                {
                    MessageBox.Show("Wybierz plik, który chcesz wczytać");
                }
                else
                {
                    GenerujPlikKonfiguracyjny(tablica_dwuwymiarowa);
                    WypiszDane(tablica_dwuwymiarowa);
                }
            }
            catch
            {
                MessageBox.Show("Wczytaj plik lub wypisz dane ponownie");
            }
        }
        private void BtnWczytajConfiga_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            sciezka2 = openFileDialog2.FileName;
            try
            {
                 WczytajConfig(sciezka2);
            }
            catch
            {
                MessageBox.Show("Zły format pliku lub błędne dane w pliku konfiguracyjnym");
            }
        }
        private void BtnUsun_Click(object sender, EventArgs e)
        {
            if (CbUsun.SelectedItem == null)
            {
                MessageBox.Show("Wybierz kolumne do usuniecia lub wypisz dane");
            }
            else
            {
                int kolumna = CbUsun.SelectedIndex;
                tablica_dwuwymiarowa = UsunKolumne(kolumna);
                if(kolumna == decyzja)
                {
                    str_decyzja = dataGridView1.Columns[decyzja+1].HeaderText;
                }
                WypiszDane(tablica_dwuwymiarowa);
            }
        }
        private void BtnSprawdzDane_Click(object sender, EventArgs e)
        {
            try
            {
                 SprawdzDane(sciezka2);
            }
            catch
            {
                MessageBox.Show("Wczytaj plik konfiguracyjny");
            }
        }
        string[,] WczytajDane(string sciezka_wczytana)
        {
            char separator = ',';
            string[] lines = File.ReadAllLines(sciezka_wczytana);
            List<int> listaUsuwania = new List<int>();

            //Sprawdzenie separatora w danym pliku 
            if (lines[0].Contains(" "))
            {
                separator = ' ';
            }
            else if (lines[0].Contains(","))
            {
                separator = ',';
            }
            else if (lines[0].Contains(":"))
            {
                separator = ':';
            }
            else if (lines[0].Contains(";"))
            {
                separator = ';';
            }
            else if (lines[0].Contains("\t"))
            {
                separator = '\t';
            }

            string[] slowo = lines[0].Split(separator);
            tablica_dwuwymiarowa = new string[lines.Length, slowo.Length];

            //Stworzenie dwuwymiarowej tablicy i zastąpienie kropki przecinkiem
            for (int i = 0; i < lines.Length; i++)
            {
                slowo = lines[i].Split(separator);
                for (int j = 0; j < slowo.Length; j++)
                {
                    slowo[j] = slowo[j].Replace('.', ',');
                    tablica_dwuwymiarowa[i, j] = slowo[j];
                }
            }

            //Wyszukanie rekordów z nieznanymi wartosciami
            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
                {
                    if (tablica_dwuwymiarowa[i, j] == "?")
                    {
                        listaUsuwania.Add(i);
                        i++;
                        j = 0;
                    }

                }
            }

            //tablica z nowymi wartosciami
            string[,] tablica_koncowa = new string[tablica_dwuwymiarowa.GetLength(0) - listaUsuwania.Count, tablica_dwuwymiarowa.GetLength(1)];

            //licznik pozwalajacy na ustawienie wartosci w prawidlowych miejscach
            int licznik = 0;

            //usuniecie rekordów z nieznanymi wartosciami
            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
                {
                    if (listaUsuwania.Contains(i))
                    {
                        i++;
                        j = 0;
                        licznik++;
                    }
                    tablica_koncowa[i-licznik, j] = tablica_dwuwymiarowa[i, j];
                }
            }

            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            CbLista.Items.Clear();
            CbUsun.Items.Clear();
            CblNormalizuj.Items.Clear();

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_koncowa.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView1.Columns.Add(column);
            }

            for (int i = 0; i < tablica_koncowa.GetLength(0); i++)
            {
                dataGridView1.Rows.Add();
            }

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_koncowa.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_koncowa.GetLength(1); j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = tablica_koncowa[i, j];
                }
            }

            //Nazewnictwo kolumn i zaznaczenie klasy decyzyjnej
            for (int i = 0; i < tablica_koncowa.GetLength(1); i++)
            {

                dataGridView1.Columns[i].HeaderText = "A" + (i + 1);
                nazwy_kolumn.Add("A" + (i + 1));
                CbLista.Items.Add("A" + (i + 1));
                CbUsun.Items.Add("A" + (i + 1));
                CblNormalizuj.Items.Add("A" + (i + 1));
                CblNormalizuj.SetItemChecked(i, true);
            }

            dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
            dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.LightBlue;

            return tablica_koncowa;
        }
        void WypiszDane(string[,] tablica_wczytana)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            CbLista.Items.Clear();
            CbUsun.Items.Clear();
            CblNormalizuj.Items.Clear();

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView1.Columns.Add(column);
            }

            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            {
                dataGridView1.Rows.Add();
            }

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            { 
                for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = tablica_wczytana[i, j];
                }
            }

            //Nazewnictwo kolumn i zaznaczenie klasy decyzyjnej
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {

                dataGridView1.Columns[i].HeaderText = nazwy_kolumn[i];
                CbLista.Items.Add(nazwy_kolumn[i]);
                CbUsun.Items.Add(nazwy_kolumn[i]);
                CblNormalizuj.Items.Add(nazwy_kolumn[i]);
                CblNormalizuj.SetItemChecked(i, true);
            }

            dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
            dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.LightBlue;
            str_decyzja = nazwy_kolumn[decyzja];
        }
        void WypiszDane2(string[,] tablica_wczytana)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Refresh();
            CbLista.Items.Clear();
            CbUsun.Items.Clear();
            CblNormalizuj.Items.Clear();

            //Stworzenie odpowiedniej dataGirdView
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {
                var column = new DataGridViewTextBoxColumn();
                dataGridView1.Columns.Add(column);
            }

            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            {
                dataGridView1.Rows.Add();
            }

            //Wypisanie danych do dataGirDView
            for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = tablica_wczytana[i, j];
                }
            }

            //Nazewnictwo kolumn i zaznaczenie klasy decyzyjnej
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {

                dataGridView1.Columns[i].HeaderText = "A" + (i + 1);
                nazwy_kolumn.Add("A" + (i + 1));
                CbLista.Items.Add("A" + (i + 1));
                CbUsun.Items.Add("A" + (i + 1));
                CblNormalizuj.Items.Add("A" + (i + 1));
                CblNormalizuj.SetItemChecked(i, true);
            }
        }
        void NormalizujDane(string[,] tablica_wczytana, double poczZakresu, double konZakresu)
        {
            tablica_normalizowana = new double[tablica_wczytana.GetLength(0), tablica_wczytana.GetLength(1)];
            double[] maxi = new double[tablica_wczytana.GetLength(1)];
            double[] mini = new double[tablica_wczytana.GetLength(1)];
            double[] przedzial = new double[tablica_wczytana.GetLength(1)];
            int licznik = 1;
            double pom_dbl;
            Dictionary<string, string> slownik = new Dictionary<string, string>();

            //Zastapienie znakow liczbami
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                {
                    //Sprawdzanie czy wartosc jest liczba
                    if (!(double.TryParse(tablica_wczytana[i, j], out pom_dbl)))
                    {
                        //Sprawdzanie czy istnieje dany klucz
                        if (slownik.ContainsKey(tablica_wczytana[i, j]))
                        {
                            foreach (var znak in slownik)
                            {
                                if (znak.Key == tablica_wczytana[i, j])
                                {
                                    tablica_wczytana[i, j] = znak.Value;
                                }
                            }
                        }
                        else
                        {
                            slownik.Add(tablica_wczytana[i, j], licznik.ToString());
                            tablica_wczytana[i, j] = licznik.ToString();
                            licznik++;
                        }
                    }
                }
                licznik = 1;
                slownik.Clear();
            }

            //Wyznaczenie wartosci maksymalnych i minimalnych do normalizacji
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                maxi[j] = double.Parse(tablica_wczytana[1, j]);
                mini[j] = double.Parse(tablica_wczytana[1, j]);
                for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                {
                    if (double.Parse(tablica_wczytana[i, j]) > maxi[j])
                    {
                        maxi[j] = double.Parse(tablica_wczytana[i, j]);
                    }
                    if (double.Parse(tablica_wczytana[i, j]) < mini[j])
                    {
                        mini[j] = double.Parse(tablica_wczytana[i, j]);
                    }
                }

            }

            //Wyzanczenie przedzialu do normalizacji
            for (int i = 0; i < tablica_wczytana.GetLength(1); i++)
            {
                przedzial[i] = maxi[i] - mini[i];
            }

            //Normalizacja tablicy
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                {
                    if (decyzja != j)
                    {
                        tablica_normalizowana[i, j] = Math.Round((poczZakresu + (((double.Parse(tablica_wczytana[i, j]) - mini[j]) / przedzial[j]) * (konZakresu - poczZakresu))), 3);
                    }
                    else
                    {
                        tablica_normalizowana[i, j] = double.Parse(tablica_wczytana[i, j]);
                    }
                }
            }

        }

        //Zapis danych do formatu CSV
        void ZapiszCSV()
        {
            //tablica aktualnych danych
            string[,] tablica = new string[dataGridView1.Rows.Count - 1, dataGridView1.Columns.Count];

            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    tablica[i, j] = dataGridView1[j, i].Value.ToString();
                }
            }

            StreamWriter sw = new StreamWriter(@".\plik.csv");
            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    sw.Write(tablica[i, j]);
                    if (j != (tablica.GetLength(1) - 1))
                    {
                        sw.Write(";");
                    }
                }
                sw.Write("\n");
            }
            sw.Close();
        }

        //Zapis danych do formatu JSON
        void ZapiszJSON()
        {
            //tablica aktualnych danych
            string[,] tablica = new string[dataGridView1.Rows.Count - 1, dataGridView1.Columns.Count];

            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    tablica[i, j] = dataGridView1[j, i].Value.ToString();
                }
            }

            StreamWriter sw = new StreamWriter(@".\plik.json");

            string json = JsonConvert.SerializeObject(tablica);
            string jsonFormatted = JValue.Parse(json).ToString(Newtonsoft.Json.Formatting.Indented);

            sw.Write(jsonFormatted);
            sw.Close();
        }

        //Zapis danych do formatu XML
        void ZapiszXML()
        {
            //tablica aktualnych danych
            string[,] tablica = new string[dataGridView1.Rows.Count - 1, dataGridView1.Columns.Count];

            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    tablica[i, j] = dataGridView1[j, i].Value.ToString();
                }
            }

            string filname = (@".\plik.xml");

            XmlTextWriter XmlWriter = new XmlTextWriter(filname, System.Text.Encoding.UTF8);

            XmlWriter.Formatting = System.Xml.Formatting.Indented;
            XmlWriter.WriteStartDocument();
            XmlWriter.WriteComment("Plik XML");
            XmlWriter.WriteStartElement("Obiekty");

            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                XmlWriter.WriteStartElement("Obiekt" + (i+1));
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    XmlWriter.WriteElementString(dataGridView1.Columns[j].HeaderText, tablica[i,j]);
                    
                }
                XmlWriter.WriteEndElement();
            }
            XmlWriter.WriteEndElement();
            XmlWriter.WriteEndDocument();
            XmlWriter.Flush();
            XmlWriter.Close();
        }
        void GenerujPlikKonfiguracyjny(string[,] tablica_wczytana)
        {
            string plik;
            double[] maxi = new double[tablica_wczytana.GetLength(1)];
            double[] mini = new double[tablica_wczytana.GetLength(1)];
            double[] przedzial = new double[tablica_wczytana.GetLength(1)];
            int licznik = 1;
            string pom = "";

            Dictionary<string, string> slownik = new Dictionary<string, string>();
            StreamWriter sw = new StreamWriter(@".\plik.txt");

            //Dodanie do ilosci wierszy i kolumn
            plik = "PLIK KONFIGURACYJNY" + "\n";
            plik = plik + "Ilosc atrybutów" + "\n";
            plik = plik + tablica_wczytana.GetLength(1) + "\n";

            plik = plik + "Klasa decyzyjna" + "\n";
            plik = plik + str_decyzja + "\n";

            plik = plik + "Przedział normalizacji" + "\n";
            plik = plik + TbZakres1.Text + " " + TbZakres2.Text + "\n";

            plik = plik + "Kolumny normalizowane" + "\n";
            for(int i = 0; i < nazwy_kolumn.Count(); i++)
            {
                plik = plik + nazwy_kolumn[i] + " ";
            }

            if (!(usuniete_kolumny.Count() == 0))
            {
                plik = plik + "\n";
                plik = plik + "Kolumny usuniete" + "\n";
                for (int i = 0; i < usuniete_kolumny.Count(); i++)
                {
                    plik = plik + usuniete_kolumny[i] + " ";
                }
            }
            else
            {
                plik = plik + "\n";
                plik = plik + "\n";
            }

            plik = plik + "\n" + "INFORMACJE O DANYCH" + "\n";
            plik = plik + "Atrybut(nazwa, typ, maks, mini, przedzial)" + "\n";
            //Wyznaczenie wartosci maksymalnych, minimalnych i przedzialu
            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                //Sprawdzenie czy kolumna jest numeryczna
                if (double.TryParse(tablica_wczytana[1, j], out maxi[j]))
                {
                    maxi[j] = double.Parse(tablica_wczytana[1, j]);
                    mini[j] = double.Parse(tablica_wczytana[1, j]);
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        if (double.Parse(tablica_wczytana[i, j]) > maxi[j])
                        {
                            maxi[j] = double.Parse(tablica_wczytana[i, j]);
                        }
                        if (double.Parse(tablica_wczytana[i, j]) < mini[j])
                        {
                            mini[j] = double.Parse(tablica_wczytana[i, j]);
                        }
                    }
                    przedzial[j] = maxi[j] - mini[j];
                    //Dodanie wartosci do pliku
                    plik = plik + dataGridView1.Columns[j].HeaderText + " " + "Numeryczny" + " " + mini[j] + " " + maxi[j] + " " + przedzial[j] + "\n";

                }
                else
                {
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        //Sprawdzanie czy istnieje dany klucz
                        if (!(slownik.ContainsKey(tablica_wczytana[i, j])))
                        {
                            slownik.Add(tablica_wczytana[i, j], licznik.ToString());
                            licznik++;
                        }
                    }
                    foreach (var znak in slownik)
                    {
                        pom = pom + " " + znak.Key + " => " + znak.Value;
                    }
                    plik = plik + dataGridView1.Columns[j].HeaderText + " " + "Nienumeryczny" + " " + pom + "\n";
                    slownik.Clear();
                    pom = "";
                    licznik = 1;
                }

            }
            sw.Write(plik);
            sw.Close();

            RtbKonfiguracyjny.Text = plik;

        }
        void WczytajConfig(string sciezka_wczytana)
        {
            string[] daneConfig = File.ReadAllLines(sciezka_wczytana);
            string plik = "";
            for(int i = 0; i < daneConfig.Length; i++)
            {
                plik = plik + daneConfig[i] + "\n";
            }

            //mozliwosc ustawienia klasy decyzyjnej
            string[] danedecyzja = daneConfig[4].Split(' ');

            for (int i = 0; i < tablica_dwuwymiarowa.GetLength(1); i++)
            {
                if ((danedecyzja[0] == dataGridView1.Columns[i].HeaderText))
                {
                    decyzja = i;
                }
            }

            //mozliwosc ustawienia zakresu normalizacji
            string[] dane_normalizacja  = daneConfig[6].Split(' ');
            double poczZakresu = double.Parse(dane_normalizacja[0]);
            double konZakresu = double.Parse(dane_normalizacja[1]);

            //mozliwosc wybrania atrybutów do normalizacji
            string[] atrybuty_normaliazja = daneConfig[8].Split(' ');
            List<int> lista_atrybutow = new List<int>();

            for(int i = 0; i < tablica_dwuwymiarowa.GetLength(1); i++)
            {
                for (int j = 0; j < atrybuty_normaliazja.GetLength(0); j++)
                {
                    if(nazwy_kolumn[i] == atrybuty_normaliazja[j])
                    {
                        lista_atrybutow.Add(i);
                    }
                }
            }

            string[,] tablica_pomniejszona;

            tablica_pomniejszona = WybierzDoNormalizacji(tablica_dwuwymiarowa, lista_atrybutow);
            NormalizujDane(tablica_pomniejszona, poczZakresu, konZakresu);

            string[,] tablica_normalizowana_str = new string[tablica_normalizowana.GetLength(0), tablica_normalizowana.GetLength(1)];

            for (int i = 0; i < tablica_normalizowana.GetLength(0); i++)
            {
                for (int j = 0; j < tablica_normalizowana.GetLength(1); j++)
                {
                    tablica_normalizowana_str[i, j] = tablica_normalizowana[i, j].ToString();
                }
            }
            WypiszDane(tablica_normalizowana_str);

            for (int i = 0; i < tablica_normalizowana.GetLength(1); i++)
            {
                dataGridView1.Columns[i].HeaderText = atrybuty_normaliazja[i];
            }

            dataGridView1.Columns[decyzja].HeaderText = "Klasa_decyzyjna";
            dataGridView1.Columns[decyzja].DefaultCellStyle.BackColor = Color.LightBlue;

            RtbKonfiguracyjny.Text = plik;
        }
        string[,] UsunKolumne(int kolumna)
        {
            string[,] tablica_koncowa = new string[tablica_dwuwymiarowa.GetLength(0), tablica_dwuwymiarowa.GetLength(1) - 1];
            int licznik = 0;
            usuniete_kolumny.Add(nazwy_kolumn[kolumna]);
            nazwy_kolumn.RemoveAt(kolumna);

            for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
            {
                //sprawdzenie ktorą kolumne należy usunąć
                if (j == kolumna)
                {
                    licznik = 1;
                }
                else
                {
                    for (int i = 0; i < tablica_dwuwymiarowa.GetLength(0); i++)
                    {
                        tablica_koncowa[i, j - licznik] = tablica_dwuwymiarowa[i, j];
                    }
                }
            }

            //Jesli usuwana kolumna jest przed klasa decyzyjna to przesuwamy w lewa strone
            if(kolumna < decyzja)
            {
                decyzja--;
            }

            return tablica_koncowa;
        }

        string[,] WybierzDoNormalizacji(string[,] tablica_wczytana, List<int> pominieteAtrybuty)
        {

            string[,] tablica_koncowa = new string[tablica_wczytana.GetLength(0), pominieteAtrybuty.Count()];
            int licznik = 0;
            //licznik pozwalajacy na przesuniecie klasy decyzynej
            int pomin = 0;

            for (int j = 0; j < tablica_wczytana.GetLength(1); j++)
            {
                //sprawdzenie ktorą kolumne należy usunąć
                if (!pominieteAtrybuty.Contains(j))
                {
                    licznik++;
                    if(j < decyzja)
                    {
                        pomin++;
                    }

                }
                else
                {
                    for (int i = 0; i < tablica_wczytana.GetLength(0); i++)
                    {
                        tablica_koncowa[i, j - licznik] = tablica_wczytana[i, j];
                    }
                }
            }
            decyzja -= pomin;
            str_decyzja = dataGridView1.Columns[decyzja].HeaderText;
            return tablica_koncowa;
        }
        string[,] odczytXML()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(sciezka);
            string[,] tablica_koncowa = new string[doc.DocumentElement.ChildNodes.Count, doc.DocumentElement.FirstChild.ChildNodes.Count];
            int rzad = 0;
            int kolumna = 0;
            foreach (XmlElement item in doc.DocumentElement.ChildNodes)
            {
                kolumna = 0;
                foreach (XmlElement it in item.ChildNodes)
                {
                    tablica_koncowa[rzad, kolumna] = it.InnerText;
                    kolumna++;
                }
                rzad++;
            }
            return tablica_koncowa;

        }
        string[,] odczytCSV()
        {
            char separator = ';';
            string[] lines = File.ReadAllLines(sciezka);

            string[] slowo = lines[0].Split(separator);
            string[,] tablica_koncowa = new string[lines.Length, slowo.Length];

            //Stworzenie dwuwymiarowej tablicy i zastąpienie kropki przecinkiem
            for (int i = 0; i < lines.Length; i++)
            {
                slowo = lines[i].Split(separator);
                for (int j = 0; j < slowo.Length; j++)
                {
                    slowo[j] = slowo[j].Replace('.', ',');
                    tablica_koncowa[i, j] = slowo[j];
                }
            }
            return tablica_koncowa;
        }
        string[,] odczytJson()
        {

            string jsonString = File.ReadAllText(sciezka);
            string[,] tablica = JsonConvert.DeserializeObject<string[,]>(jsonString);

            return tablica;
        }
        void SprawdzDane(string sciezka_wczytana)
        {
            string[] daneConfig = File.ReadAllLines(sciezka2);
            string plik = "";
            string[] ilosc_atrybutow = daneConfig[2].Split(' ');
            string[] typy = new string[tablica_dwuwymiarowa.GetLength(0)];
            string[] typy_tablicy = new string[tablica_dwuwymiarowa.GetLength(0)];
            int index = 0;
            int licznik = 0;
            double pom_dbl;

            if (tablica_dwuwymiarowa.GetLength(1) == int.Parse(ilosc_atrybutow[0]))
            {
                plik = "Liczba atrybutów sie zgadza\n";
            }
            else
            {
                plik = "Liczba atrybutów sie nie zgadza\n";
            }

            for (int j = 0; j < tablica_dwuwymiarowa.GetLength(1); j++)
            {
                    //Sprawdzanie czy wartosc jest liczba
                 if (double.TryParse(tablica_dwuwymiarowa[0, j], out pom_dbl))
                 {
                    typy_tablicy[j] = "Numeryczny";
                 }
                 else
                 {
                    typy_tablicy[j] = "Nienumeryczny";
                }
             }

            for (int i = 13; i < daneConfig.Length; i++)
            {
                string[] pom = daneConfig[i].Split(' ');
                if(pom[1] == typy_tablicy[index])
                {
                    licznik++;
                }
                index++;
            }
            if(licznik == tablica_dwuwymiarowa.GetLength(1))
            {
                plik = plik + "Typy danych zgadzaja sie\n";
            }
            else
            {
                plik = plik + "Typt danych nie zgadzaja sie\n";
            }

            RtbSprawdzenie.Text = plik;
        }
    }
}
